package com.ecodia.android.snippets;

public class SnippetString {

	/*

	Java:
		getString(R.string.app_name)


	XML:
        android:text="@string/hello"
	  
	 */
}

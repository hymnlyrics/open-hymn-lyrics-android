package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

import java.util.ArrayList;

public class StanzaUtil {
	
	public static String refrainBetweenAllStanzas(String song) {
		ArrayList<String> stanzas = StanzaUtil.getStanzas(song);
		String refrain = StanzaUtil.getRefrain(song);

		StringBuffer newSong = new StringBuffer();
		for (String stanza : stanzas) {
			newSong.append(stanza);
			newSong.append("\n");
			newSong.append(refrain);
			newSong.append("\n");
		}

		song = newSong.toString();
		
		return song;
	}
	

	private static String getRefrain(String song) {
		StringBuffer refrain = new StringBuffer();
		String[] lines = song.split("\n");
		boolean inRefrain = false;

		for (int i = 0; i < lines.length; i++) {
			String line = lines[i].trim();
			if (line.startsWith("Refrain:")) {
				inRefrain = true;
			} else if (inRefrain && line.length() == 0) {
				break;
			} else if (inRefrain) {
				refrain.append(line + "\n");
			}
		}

		return refrain.toString();
	}

	private static ArrayList<String> getStanzas(String song) {
		ArrayList<String> stanzas = new ArrayList<String>();
		StringBuffer stanza = new StringBuffer();
		String[] lines = song.split("\n");
		boolean inStanza = true;

		for (int i = 0; i < lines.length; i++) {
			String line = lines[i].trim();
			if (line.startsWith("Refrain:")) {
				inStanza = false;
			} else if (line.length() == 0) {
				if (inStanza) {
					stanzas.add(stanza.toString());
					stanza = new StringBuffer();
				}
				inStanza = true;
			} else if (inStanza) {
				stanza.append(line + "\n");
			}
		}
		stanzas.add(stanza.toString());

		return stanzas;
	}

	public static void main(String[] args) {

		String song = "Is there a heart o'er bound by sorrow?\n"
				+ "Is there a life weighed down by care?\n"
				+ "Come to the cross each burden bearing.\n"
				+ "All our anxiety leave it there.\n" + "\n" + "Refrain:\n"
				+ "All your anxiety, all your care,\n"
				+ "Bring to the mercy seat leave it there;\n"
				+ "Never a burden He cannot bear,\n"
				+ "Never a friend like Jesus.\n" + "\n"
				+ "No other friend so keen to help you,\n"
				+ "No other friend so quick to hear.\n"
				+ "No other place to leave your burden;\n"
				+ "No other one to hear our prayer.\n" + "\n"
				+ "Come then at once; delay no longer!\n"
				+ "Heed His entreaty kind and sweet.\n"
				+ "You need not fear a disappointment;\n"
				+ "You shall find peace at the mercy seat.\n";

		ArrayList<String> stanzas = null;
		stanzas = StanzaUtil.getStanzas(song);
		// System.out.println(refrain);

		for (String stanza : stanzas) {
			System.out.println(stanza);
			System.out.println("--");
		}
	}

}

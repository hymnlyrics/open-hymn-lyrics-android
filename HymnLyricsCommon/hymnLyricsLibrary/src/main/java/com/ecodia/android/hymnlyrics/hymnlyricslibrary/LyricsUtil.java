package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

public class LyricsUtil {

	public static String buildLyrics(String author, String keywords, String lyrics, String tuneLabel, String tune, boolean showChords, boolean refrainBetweenStanzas) {
		String text = "";
		if (keywords != null && keywords.length() > 0) {
			keywords = keywords.substring(0, keywords.length()-1);
			text += keywords + "\n";
		}
		if (author != null && author.length() > 0) {
			text += author + "\n";
		}
		if (tune != null && tune.length() > 0) {
			text += tuneLabel + ": " + tune + "\n";
		}
		text += "\n";
		
		if (refrainBetweenStanzas) {
			lyrics = StanzaUtil.refrainBetweenAllStanzas(lyrics);
		}

		if (showChords) {
			text += ChordUtil.showChords(lyrics);
		}
		else {
			text += ChordUtil.stripChords(lyrics);
		}
		
		return text;
	}
}

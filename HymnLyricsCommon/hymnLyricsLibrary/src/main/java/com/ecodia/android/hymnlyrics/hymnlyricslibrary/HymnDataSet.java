package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

import java.util.ArrayList;

public class HymnDataSet {
	private ArrayList<String> titlesList;
	private ArrayList<String> tuneList;
	private ArrayList<String> lastUpdateList;
	private ArrayList<String> authorList;
	private ArrayList<String> keywordList;
	private ArrayList<String> backgroundList;
	private ArrayList<String> lyricsList;

	public HymnDataSet(ArrayList<String> titles,
			ArrayList<String> tunes,
			ArrayList<String> lastUpdate,
			ArrayList<String> authors, ArrayList<String> keywords,
			ArrayList<String> lyrics,
			ArrayList<String> background
			) {
		titlesList = titles;
		setTuneList(tunes);
		lastUpdateList = lastUpdate;
		authorList = authors;
		keywordList = keywords;
		setBackgroundList(background);
		lyricsList = lyrics;
		setBackgroundList(background);
	}

	public ArrayList<String> getTitlesList() {
		return titlesList;
	}

	public void setTitlesList(ArrayList<String> titlesList) {
		this.titlesList = titlesList;
	}

	public ArrayList<String> getAuthorList() {
		return authorList;
	}

	public void setAuthorList(ArrayList<String> authorList) {
		this.authorList = authorList;
	}

	public ArrayList<String> getKeywordList() {
		return keywordList;
	}

	public void setKeywordList(ArrayList<String> keywordList) {
		this.keywordList = keywordList;
	}

	public ArrayList<String> getLyricsList() {
		return lyricsList;
	}

	public void setLyricsList(ArrayList<String> lyricsList) {
		this.lyricsList = lyricsList;
	}

	public ArrayList<String> getLastUpdateList() {
		return lastUpdateList;
	}

	public void setLastUpdateList(ArrayList<String> lastUpdateList) {
		this.lastUpdateList = lastUpdateList;
	}

	public ArrayList<String> getTuneList() {
		return tuneList;
	}

	public void setTuneList(ArrayList<String> tuneList) {
		this.tuneList = tuneList;
	}

	public ArrayList<String> getBackgroundList() {
		return backgroundList;
	}

	public void setBackgroundList(ArrayList<String> backgroundList) {
		this.backgroundList = backgroundList;
	}

}

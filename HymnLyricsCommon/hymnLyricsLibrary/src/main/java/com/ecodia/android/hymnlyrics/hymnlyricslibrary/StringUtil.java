package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

public class StringUtil {
	public static String stripHymnNumber(String title) {
		if (title != null && title.length() > 0 &&
				title.indexOf('-') > 0 && Character.isDigit(title.charAt(0))) {
			String text = title.substring(title.indexOf('-') + 1);
			text = text.trim();
			return text;
		} else {
			return title;
		}
	}
}

package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.HymnDataSet;

public interface IHymnLyricsData {
	public String[] getCategories();
	public HymnDataSet getInsertDataSet();
	public void setCategories(String[] cats);
}
package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

public class ChordUtil {
	public static String showChords(String in) {
		StringBuffer chords = null;
		StringBuffer lyrics = null;
		StringBuffer out = new StringBuffer();

		String[] lines = in.split("\n");
		int mode = 0;

		for (int i = 0; i < lines.length; i++) {
			boolean hasChord = false;
			String line = lines[i].trim();

			chords = new StringBuffer();
			lyrics = new StringBuffer();
			int cl = 0;
			for (int j = 0; j < line.length(); j++) {
				char c = line.charAt(j);
				if (mode == 0) {
					if (c == '<') {
						mode = 1;
						hasChord = true;
					} else {
						if (cl == 0) {
							chords.append(" ");
						} else {
							cl--;
						}
						lyrics.append(c);
					}
				} else if (mode == 1) {
					if (c == '>') {
						mode = 0;
					} else {
						chords.append(c);
						cl++;
					}

				}
			}

			if (line.length() > 0 && hasChord) {
				out.append(chords);
				out.append("\n");
			}
			out.append(lyrics);
			out.append("\n");

		}
		return out.toString();
	}

	public static String stripChords(String in) {
		StringBuffer lyrics = null;
		StringBuffer out = new StringBuffer();

		String[] lines = in.split("\n");
		int mode = 0;

		for (int i = 0; i < lines.length; i++) {
			String line = lines[i];

			lyrics = new StringBuffer();
			for (int j = 0; j < line.length(); j++) {
				char c = line.charAt(j);
				if (mode == 0) {
					if (c == '<') {
						mode = 1;
					} else {
						lyrics.append(c);
					}
				} else if (mode == 1) {
					if (c == '>') {
						mode = 0;
					} else {
					}
				}
			}
			out.append(lyrics);
			out.append("\n");

		}
		return out.toString();
	}

	public static void main(String[] args) {

		String song = "<C>O soul, <D>are you weary and <E>troubled?\n"
				+ "\n"
				+ "<F#>No light in the <Am>darkness you see?\n"
				+ "<Bb>His perfect <C>salvation to <F#m>tell!\n";

		System.out.println(ChordUtil.stripChords(song));
		System.out.println(ChordUtil.showChords(song));

	}
}

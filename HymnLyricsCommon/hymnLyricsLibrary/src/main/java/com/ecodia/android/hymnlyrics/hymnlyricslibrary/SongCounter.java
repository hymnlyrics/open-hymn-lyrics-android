package com.ecodia.android.hymnlyrics.hymnlyricslibrary;

import java.util.ArrayList;

public class SongCounter {

	public static int getChordCount(HymnDataSet insertDataSet) {
		ArrayList<String> lyricsList = insertDataSet.getLyricsList();
		ArrayList<String> titlesList = insertDataSet.getTitlesList();
		int songIndex = -1;
		int chordCount = 0;
		
		NextSong:
		for (String lyrics : lyricsList) {

			songIndex++;

			if (lyrics.indexOf("<") == -1) {
				continue;
			}
		
			chordCount++;
			
			int chord = -1;
					
			for(int i=0; i < lyrics.length(); i++) {
				Character c = lyrics.charAt(i);
				if (c.equals('<')) {
					if (chord != -1) {
						System.out.println(titlesList.get(songIndex) + " is missing a > at index " + i);
						continue NextSong;
					}
					chord = 0;
				}
				else if (c.equals('>')) {
					chord = -1;
				}
				else if (chord > -1) {
					chord++;
				}
				else if (chord > 4) {
					System.out.println(titlesList.get(songIndex) + " chord name is too long at index " + i);
					continue NextSong;
				}
			}
		}

		return chordCount;
	}

	public static int getDuplicateTitleCount(HymnDataSet insertDataSet) {

		ArrayList<String> titlesList = insertDataSet.getTitlesList();

		for (String lyrics : titlesList) {
			int count = 0;
			for (String lyrics2 : titlesList) {
				if (lyrics.equalsIgnoreCase(lyrics2)) {
					count++;
					if (count > 1) {
						return count;
					}
				}
			}
		}

		return 0;
	}

	public static int getSongCount(HymnDataSet insertDataSet) throws Exception {
		ArrayList<String> titlesList = insertDataSet.getTitlesList();
		ArrayList<String> tuneList = insertDataSet.getTuneList();
		ArrayList<String> authorList = insertDataSet.getAuthorList();
		ArrayList<String> keywordList = insertDataSet.getKeywordList();
		ArrayList<String> lyricsList = insertDataSet.getLyricsList();
		ArrayList<String> lastUpdateList = insertDataSet.getLastUpdateList();
		ArrayList<String> backgroundList = insertDataSet.getBackgroundList();

		if (titlesList.size() != lastUpdateList.size()
				|| lastUpdateList.size() != authorList.size()
				|| lastUpdateList.size() != tuneList.size()
				|| authorList.size() != keywordList.size()
				|| authorList.size() != backgroundList.size()
				|| keywordList.size() != lyricsList.size()) {
			System.out.println("Titles: " + titlesList.size());
			System.out.println("Tunes: " + tuneList.size());
			System.out.println("Last Update: " + lastUpdateList.size());
			System.out.println("Authors: " + authorList.size());
			System.out.println("Keywords: " + keywordList.size());
			System.out.println("Lyrics: " + lyricsList.size());
			System.out.println("Background: " + backgroundList.size());

			throw new Exception("Error in sizes!!!");
		}

		return titlesList.size();
	}
}

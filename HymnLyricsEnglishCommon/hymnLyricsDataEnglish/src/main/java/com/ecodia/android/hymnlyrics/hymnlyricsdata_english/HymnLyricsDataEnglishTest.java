package com.ecodia.android.hymnlyrics.hymnlyricsdata_english;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.HymnDataSet;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.IHymnLyricsData;

public class HymnLyricsDataEnglishTest implements IHymnLyricsData {

    private String[] categories = {};

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] cats) {
        this.categories = cats;
    }

    private void loadCategories(ArrayList<String> keywordList) {
        TreeSet<String> cats = new TreeSet<String>();

        for (String keywords : keywordList) {

            String[] category = keywords.split(",");
            for (int x = 0; x < category.length; x++) {
                cats.add(category[x]);
            }
        }

        ArrayList<String> list = new ArrayList<String>();

        Iterator<String> cat = cats.iterator();
        while (cat.hasNext()) {
            list.add(cat.next());
        }
        categories = list.toArray(new String[list.size()]);
    }

    public HymnDataSet getInsertDataSet() {

        ArrayList<String> titlesList = new ArrayList<String>();
        ArrayList<String> tuneList = new ArrayList<String>();
        ArrayList<String> lastUpdateList = new ArrayList<String>();
        ArrayList<String> authorList = new ArrayList<String>();
        ArrayList<String> keywordList = new ArrayList<String>();
        ArrayList<String> lyricsList = new ArrayList<String>();
        ArrayList<String> backgroundList = new ArrayList<String>();

        getInsertDataSetTest(titlesList, tuneList, lastUpdateList, authorList, keywordList,
                lyricsList, backgroundList);

        loadCategories(keywordList);

        HymnDataSet dataset = new HymnDataSet(titlesList, tuneList,
                lastUpdateList, authorList, keywordList, lyricsList, backgroundList);

        return dataset;
    }

    public void getInsertDataSetTest(ArrayList<String> titlesList, ArrayList<String> tuneList,
                                  ArrayList<String> lastUpdateList, ArrayList<String> authorList, ArrayList<String> keywordList,
                                  ArrayList<String> lyricsList, ArrayList<String> backgroundList) {

        // Test with 3 to delete all and reload
        // Test with 5 to import only new songs
        int count = 5;

        titlesList.add("Abba, Father! We Approach Thee");
        lastUpdateList.add("2018-01-01");
        tuneList.add("");
        keywordList.add("");
        authorList.add("James G Deck");
        backgroundList.add("");
        lyricsList.add("\"Abba, Father!\" We approach Thee\n"
                + "In our Savior's precious Name;\n"
                + "We, Thy children, here assembling,\n"
                + "Access to Thy presence claim;\n"
                + "From our sins His blood hath washed us,\n"
                + "'Tis through Him our souls draw near,\n"
                + "And Thy Spirit, too, hath taught us,\n"
                + "\"Abba, Father,\" Name so dear.\n"
                + "\n"
        );

        titlesList.add("Believe");
        lastUpdateList.add("2018-07-01");
        tuneList.add("");
        keywordList.add("");
        authorList.add("Henry F. Lyte");
        backgroundList.add("");
        lyricsList.add("Abide with me; fast falls the eventide;\n"
                + "The darkness deepens; Lord with me abide.\n"
                + "When other helpers fail and comforts flee,\n"
                + "Help of the helpless, O abide with me.\n"
        );

        if (count > 2) {
            titlesList.add("Creatures Of Our God And King");
            lastUpdateList.add("2018-12-01");
            tuneList.add("");
            keywordList.add("Popular,");
            authorList.add("Saint Francis of Assisi");
            backgroundList.add("");
            lyricsList.add("<G>All creatures <C2>of our God and <D>King,\n"
                    + "<G>Lift up your <C2>voice and with us <D>sing,\n"
                    + "<C2>Alle<G>luia! <C2>Alle<D>luia!\n"
                    + "<G>Thou burning <C2>sun with golden <D>beam,\n"
                    + "<G>Thou silver <C2>moon with softer <D>gleam,\n"
                    + "\n"
            );
        }

        if (count > 3) {
            titlesList.add("Deck Thyself, My Soul, With Gladness");
            lastUpdateList.add("2019-12-4");
            tuneList.add("");
            keywordList.add("");
            authorList.add("");
            backgroundList.add("");
            lyricsList.add("Deck thyself, my soul, with gladness,\n"
                    + "leave the gloomy haunts of sadness,\n"
                    + "come into the daylight's splendor,\n"
                    + "there with joy thy praises render\n"
                    + "unto him whose grace unbounded\n"
                    + "hath this wondrous banquet founded;\n"
                    + "high o'er all the heavens he reigneth,\n"
                    + "yet to dwell with thee he deigneth.\n"
                    + "\n"
            );
        }

        if (count > 4) {
            titlesList.add("Gazing On The Lord In Glory");
            lastUpdateList.add("2018-11-03");
            tuneList.add("");
            keywordList.add("");
            authorList.add("C Thompson");
            backgroundList.add("");
            lyricsList.add("Gazing on the Lord in glory,\n"
                    + "While our hearts in worship bow,\n"
                    + "There we read the wondrous story\n"
                    + "Of the cross—its shame and woe.\n"
                    + "\n"
            );
        }

        if (count > 5) {
            titlesList.add("Gazing On The Lord In Glory");
            lastUpdateList.add("2018-11-03");
            tuneList.add("");
            keywordList.add("");
            authorList.add("C Thompson");
            backgroundList.add("");
            lyricsList.add("Gazing on the Lord in glory,\n"
                    + "While our hearts in worship bow,\n"
                    + "There we read the wondrous story\n"
                    + "Of the cross—its shame and woe.\n"
                    + "\n"
            );
        }
    }

}
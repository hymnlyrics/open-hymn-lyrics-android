package com.ecodia.android.hymnlyrics.hymnlyricsdata_english;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.HymnDataSet;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.IHymnLyricsData;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ExportHymnData {

    private static final String EXPORT_FILE = "songs.txt";
    private static final String COMPRESS_FILE = "songs_compressed.txt";

    private static final int MAX_EXPORT_SONGS = 10000;
    private static final boolean EXPORT_COMPRESS_FILE = false;

    public static void main(String args[]) {

//        uncompressTest();

        System.out.println("Exporting to " + EXPORT_FILE + "...");
        if (EXPORT_COMPRESS_FILE) {
            System.out.println("Exporting to " + COMPRESS_FILE + "...");
        }

        IHymnLyricsData hymnLyricsData = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsData.getInsertDataSet();

        int size = hymnDataSet.getTitlesList().size();

        if (size > MAX_EXPORT_SONGS) {
            size = MAX_EXPORT_SONGS;
        }

        writeToFile(hymnDataSet, size);

        System.out.println("Songs exported: " + size);

    }

    private static void writeToFile(HymnDataSet hymnDataSet, int size) {

        File file = new File(EXPORT_FILE);

        StringBuffer output = new StringBuffer();

        for (int i = 0; i < size; i++) {
            output.append("Title: " + hymnDataSet.getTitlesList().get(i) + "\n");
            output.append("Author: " + hymnDataSet.getAuthorList().get(i) + "\n");
            output.append("Keywords: " + hymnDataSet.getKeywordList().get(i) + "\n");
            output.append("Tune: " + hymnDataSet.getTuneList().get(i) + "\n");
            output.append("Last update: " + hymnDataSet.getLastUpdateList().get(i) + "\n");
            output.append("Background: " + hymnDataSet.getBackgroundList().get(i) + "\n");
            output.append("Lyrics:\n");
            output.append(hymnDataSet.getLyricsList().get(i) + "\n");
            output.append("---\n");
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(output.toString().getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (EXPORT_COMPRESS_FILE) {
            file = new File(COMPRESS_FILE);
            try {
                fos = new FileOutputStream(file);
                //            fos.write(base64encode(output.toString()).getBytes());
                fos.write(base64encode(compress(output.toString())).getBytes());
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void compressTest() {
        String str = "Christmas compress test string";

        String compress = compress(str);
        String encode = base64encode(compress);

        System.out.println("Encode: " + encode);
    }


    private static void uncompressTest() {
        String str = "H4sIAAAAAAAAEwvJyCxWKEktLlEozsgvzUlRKCjKTylNTlVIVCguKcrMSwcAmiQ2LSEAAAA=";

        byte[] bytes = base64decode(str);
        String decompress = decompress(bytes);

        System.out.println(decompress);
    }


    static String compress(String str) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            GZIPOutputStream gzip = new GZIPOutputStream(out);
            gzip.write(str.getBytes());
            gzip.close();
            return out.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String decompress(byte[] bytes) {
        GZIPInputStream gis = null;
        try {
            gis = new GZIPInputStream(new ByteArrayInputStream(bytes));
            BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8"));
            String outStr = "";
            String line;
            while ((line = bf.readLine()) != null) {
                outStr += line;
            }
            return outStr;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.O)
    static String base64encode(String str) {
        try {
            return Base64.getEncoder().encodeToString(str.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.O)
    static byte[] base64decode(String str) {
        try {
            return Base64.getDecoder().decode(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
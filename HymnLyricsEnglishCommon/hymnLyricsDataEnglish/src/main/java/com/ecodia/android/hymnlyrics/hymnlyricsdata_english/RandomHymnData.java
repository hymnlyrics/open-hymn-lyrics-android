package com.ecodia.android.hymnlyrics.hymnlyricsdata_english;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.HymnDataSet;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.IHymnLyricsData;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

public class RandomHymnData {

    private static final String EXPORT_FILE = "docs/random.txt";

    public static void main(String args[]) {

        System.out.println("Exporting to " + EXPORT_FILE + "...");

        int count = 5000;

        writeToFile(count);

        System.out.println("Songs exported");

    }

    private static void writeToFile(int count) {

        File file = new File(EXPORT_FILE);

        StringBuffer output = new StringBuffer();

        Random random = new Random();

        for (int i = 0; i < count; i++) {
            output.append("Title: " + generateRandomWords(random.nextInt(5) + 1) + "\n");
            output.append("Author: " + generateRandomWords(1) + "\n");
            output.append("Lyrics:\n");
            output.append(generateRandomWords(random.nextInt(50) + 50) + "\n");
            output.append("---\n");
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(output.toString().getBytes());
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String generateRandomWords(int numberOfWords)
    {
        StringBuffer randomString = new StringBuffer();
        Random random = new Random();
        for(int i = 0; i < numberOfWords; i++)
        {
            char[] word = new char[random.nextInt(8)+3]; // words of length 3 through 10. (1 and 2 letter words are boring.)
            for(int j = 0; j < word.length; j++)
            {
                word[j] = (char)('a' + random.nextInt(26));
            }
            randomString.append(new String(word)).append(" ");
        }
        return randomString.toString().trim();
    }
}

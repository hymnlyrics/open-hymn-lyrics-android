package com.ecodia.android.hymnlyrics.hymnlyricsdata_english.test;

import com.ecodia.android.hymnlyrics.hymnlyricsdata_english.HymnLyricsDataEnglish;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.HymnDataSet;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.SongCounter;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnglishSongTest {

    @Test
    public void checkSongCount() throws Exception {

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        int size = SongCounter.getSongCount(hymnDataSet);
        System.out.println("Total songs: " + size);
        assert (size == 2190);
    }

    @Test
    public void checkCategories() {
        System.out.println("Categories:");

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        String[] categories = hymnLyricsDataEnglish.getCategories();

        for (String category : categories) {
            System.out.print(category + " ");
        }
        System.out.println("");
        assert(categories.length == 13);
    }

    @Test
    public void checkChordCount() {

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        int size = SongCounter.getChordCount(hymnDataSet);
        System.out.println("Chords: " + size);
        assert (size == 91);
    }

    @Test
    public void checkDuplicateTitles() {

        System.out.println("Duplicate titles:");

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        assert (SongCounter.getDuplicateTitleCount(hymnDataSet) == 0);
    }

    @Test
    public void checkSimilarTitles() {

        System.out.println("Similar titles:");

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        TreeSet<String> titles = new TreeSet<String>();
        int count = 0;

        for (String title : hymnDataSet.getTitlesList()) {
            String shortTitle = title.replaceAll("[,.!':?]", "");

            if (titles.contains(shortTitle)) {
                System.out.println(title);
                count++;
            }
            else {
                titles.add(shortTitle);
            }
        }

        assert (count == 0);
    }

    @Test
    public void checkNoEncodedData() {

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        System.out.println("Encoded data:");
        boolean found = false;
        int count = 0;
        for (String lyrics : hymnDataSet.getLyricsList()) {
            if (lyrics.contains("&") || lyrics.contains("â€™")) {
                System.out.println(hymnDataSet.getTitlesList().get(count));
                found = true;
            }
            count++;
        }
        count = 0;
        for (String title : hymnDataSet.getTitlesList()) {
            if (title.contains("&")) {
                System.out.println(title);
                found = true;
            }
            count++;
        }
        assert (!found);
    }

    @Test
    public void checkCopyright() {

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        System.out.println("Copyright:");
        boolean found = false;
        int count = 0;
        for (String lyrics : hymnDataSet.getLyricsList()) {
            if (lyrics.contains("copyright")) {
                System.out.println(hymnDataSet.getTitlesList().get(count));
                found = true;
            }
            count++;
        }
        assert (!found);
    }

    @Test
    public void checkAuthor() {

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        System.out.println("Author:");
        boolean found = false;
        int count = 0;
        for (String author : hymnDataSet.getAuthorList()) {
            if (author.contains("Author")) {
                System.out.println(hymnDataSet.getTitlesList().get(count));
                found = true;
            }
            count++;
        }
        assert (!found);
    }

    @Test
    public void checkTitleCapitalization() {
        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        System.out.println("Title capitalization:");

        boolean passed = true;
        for (String title : hymnDataSet.getTitlesList()) {
            boolean shouldBeCapital = true;
            for (char c : title.toCharArray()) {
                if (Character.isLowerCase(c) && shouldBeCapital) {
                    System.out.println(title);
                    passed = false;
                    break;
                }

                if (" (".indexOf(c) != -1) {
                    shouldBeCapital = true;
                } else {
                    shouldBeCapital = false;
                }
            }
        }
        assert (passed);
    }

    @Test
    public void checkNoTitleStartsWithA() {
        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        System.out.println("Songs start with article:");

        boolean passed = true;
        for (String title : hymnDataSet.getTitlesList()) {
            if (title.startsWith("A ") || title.startsWith("An ") || title.startsWith("The ")) {
                System.out.println(title);
                passed = false;
            }
        }
        assert (passed);
    }

    @Test
    public void checkLastUpdateFormat() {

        System.out.println("Last update format:");

        HymnLyricsDataEnglish hymnLyricsDataEnglish = new HymnLyricsDataEnglish();
        HymnDataSet hymnDataSet = hymnLyricsDataEnglish.getInsertDataSet();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        boolean hasErrors = false;

        for (String lastUpdate : hymnDataSet.getLastUpdateList()) {
            try {
                if (lastUpdate.length() > 0) {
                    Date date = format.parse(lastUpdate);
                }
            } catch (ParseException e) {
                hasErrors = true;
                System.out.println(lastUpdate);
            }
        }
        assert(!hasErrors);
    }
}

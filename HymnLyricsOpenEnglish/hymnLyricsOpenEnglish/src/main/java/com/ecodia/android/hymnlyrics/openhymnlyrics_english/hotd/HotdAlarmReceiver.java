package com.ecodia.android.hymnlyrics.openhymnlyrics_english.hotd;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class HotdAlarmReceiver extends BroadcastReceiver {

	public static final String HOTD_ALARM = "HOTD_ALARM";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			new HymnOfTheDay(context).showAppNotification();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

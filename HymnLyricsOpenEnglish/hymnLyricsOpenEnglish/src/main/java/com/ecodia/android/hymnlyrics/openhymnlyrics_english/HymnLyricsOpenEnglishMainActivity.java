package com.ecodia.android.hymnlyrics.openhymnlyrics_english;

import com.ecodia.android.hymnlyrics.hymnlyricsdata_english.HymnLyricsDataEnglish;
import com.ecodia.android.hymnlyrics.openhymnlyrics.HymnLyricsPlusCoreActivity;
import com.ecodia.android.openhymnlyrics.R;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;

public class HymnLyricsOpenEnglishMainActivity extends Activity {

    private static final String TAG = HymnLyricsOpenEnglishMainActivity.class.getCanonicalName();

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(TAG, "Config: debug_mode=" + getString(R.string.debug_mode));

		HymnLyricsPlusCoreActivity.hymnLyricsData = new HymnLyricsDataEnglish();
		HymnLyricsPlusCoreActivity.languageCode = null;
		HymnLyricsPlusCoreActivity.locale = null;
		HymnLyricsPlusCoreActivity.fontSizeTitle = 18;
		HymnLyricsPlusCoreActivity.fontSizeText = 16;
		HymnLyricsPlusCoreActivity.packageName = getString(R.string.app_store_package);

		HymnLyricsPlusCoreActivity.supportText = "Open Source Hymn Lyrics<br/><br/>" +
				"Code is available at <a href='https://gitlab.com/hymnlyrics/open-hymn-lyrics'>Gitlab</a><br/><br/>" +
				"Report an issue: <a href='https://gitlab.com/hymnlyrics/open-hymn-lyrics/-/issues'>Issue Tracker</a><br/><br/>" +
				"Google discussion forum: <a href='https://groups.google.com/g/open-hymn-lyrics-support'>Open Hymn Lyrics Support</a> <br/><br/>" +
				"Email support: <a href='mailto:otseng@ecodia.com?subject=Open Hymn Lyrics Feedback'>otseng@ecodia.com</a><br/>";
		
		HymnLyricsPlusCoreActivity.privacyText = "<a href='https://ecodia.com/phramework/pw/module/site/view_page.php?id=8'>Privacy Policy</a><br/>";

		HymnLyricsPlusCoreActivity.fontName = null;
		HymnLyricsPlusCoreActivity.version = getString(R.string.version);

		Intent intent = new Intent(this, HymnLyricsPlusCoreActivity.class);

	    Bundle extras = getIntent().getExtras();
		int open_id = 0;
		if (extras != null) {
			open_id = extras.getInt("open_id");
		}
        intent.putExtra("open_id", open_id);

        startActivity(intent);
        finish();
	}
}

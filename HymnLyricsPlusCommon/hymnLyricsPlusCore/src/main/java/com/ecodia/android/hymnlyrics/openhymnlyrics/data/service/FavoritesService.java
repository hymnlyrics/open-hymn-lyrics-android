package com.ecodia.android.hymnlyrics.openhymnlyrics.data.service;

import java.util.ArrayList;

import com.ecodia.android.hymnlyrics.openhymnlyrics.data.dao.FavoritesDAO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.database.DatabaseManager;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.FavoritesMO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class FavoritesService {

	private static final String TAG = FavoritesService.class.getCanonicalName();

	SQLiteDatabase db = null;
	Context context;

	public FavoritesService(Context context) {

		try {
			this.context = context;
			DatabaseManager instance = DatabaseManager.getInstance(context);
			this.db = instance.getDb();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}
	public long create(FavoritesMO FavoritesMO) {
		try {
			return FavoritesDAO.create(db, FavoritesMO);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	public long update(FavoritesMO FavoritesMO) {
		try {
			return FavoritesDAO.update(db, FavoritesMO);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	public void delete(long rowID) {
		try {
			FavoritesDAO.delete(db, rowID);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteAll() {
		try {
			FavoritesDAO.deleteAll(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public ArrayList<FavoritesMO> retrieveAll() {
		try {
			return FavoritesDAO.retrieveAll(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

}

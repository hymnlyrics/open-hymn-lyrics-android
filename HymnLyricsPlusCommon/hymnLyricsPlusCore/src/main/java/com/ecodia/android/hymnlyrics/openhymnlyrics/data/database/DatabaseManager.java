package com.ecodia.android.hymnlyrics.openhymnlyrics.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseManager {
	private static SQLiteDatabase db;
	private static DatabaseManager databaseManager = null;
	
	private DatabaseManager(Context context) {
		DatabaseHelper databaseHelper = new DatabaseHelper(context);
		DatabaseManager.db = databaseHelper.getWritableDatabase();
	}
	
	public static DatabaseManager getInstance(Context context) {
		if (databaseManager == null) {
			databaseManager = new DatabaseManager(context);
		}
		return databaseManager;
	}

	public static SQLiteDatabase getDb() {
		return db;
	}
}

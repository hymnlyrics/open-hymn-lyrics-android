package com.ecodia.android.hymnlyrics.openhymnlyrics.hotd;

import java.util.Random;

import com.ecodia.android.hymnlyrics.openhymnlyrics.R;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.service.HymnLyricsService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class HymnOfTheDay {

	private static final String TAG = HymnOfTheDay.class.getCanonicalName();
	
	Context context;

	public HymnOfTheDay(Context context) {
		this.context = context;
	}

	public void showAppNotification() {
		
		HymnLyricsService service = new HymnLyricsService(context);
		int min = service.getMinId();
		int max = service.getMaxId();
		int range = max - min;

		Random random = new Random();
		HymnLyricsMO hymn = null;
		
		for (int i=0; i<100; i++) {
			int id = random.nextInt(range) + min;
			hymn = service.retrieveById(id);
			if (hymn != null ) break;
		} 
		
		if (hymn == null) {
			hymn = service.retrieveById(min);
		}

		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager nm = (NotificationManager) context
				.getSystemService(ns);

		int icon = R.drawable.icon;
		CharSequence text = "Hymn Of The Day";
		long when = System.currentTimeMillis();

//	    Intent intent = new Intent(context, IncomingMessageInterstitial.class);
//	    intent.putExtra("id", hymn.getId());
//	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//	    PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
//	            intent, PendingIntent.FLAG_CANCEL_CURRENT);
	    
		Intent intent = HymnOfTheDay.makeMessageIntent(context, hymn.getId());
	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
	            intent, PendingIntent.FLAG_CANCEL_CURRENT);

		Notification notification = new Notification(icon, text, when);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
//		notification.setLatestEventInfo(context, text, hymn.getTitle(), contentIntent);
		
		nm.notify(R.string.app_name, notification);
	}

	static Intent makeMessageIntent(Context context, int id) {
		HymnLyricsService service = new HymnLyricsService(context);
		HymnLyricsMO hymn = service.retrieveById(id);

		try {
			String hotd = "com.ecodia.android.hymnlyrics.hymnlyricsplus_english.HymnLyricsPlusEnglishMainActivity";
			Log.d(TAG, "Creating HOTD Intent: " + hotd);
			Intent intent = new Intent(context, Class.forName(hotd));
			intent.putExtra("open_id", hymn.getId());
			return intent;
		}
		catch (Exception e) {
			e.printStackTrace();
			Log.d(TAG, e.getMessage());
		}
		return null;
	}

}

package com.ecodia.android.hymnlyrics.openhymnlyrics;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.ChordUtil;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.StanzaUtil;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.service.HymnLyricsService;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class SingleHymnActivity extends Activity {

	private int hymnId;
	private String title;
	private String author;
	private String lyrics;
	private boolean favorite;
	HymnLyricsMO hymn;

	private View view;

	private static final int DIALOG_MESSAGE = 10;
	private static final int DIALOG_CUSTOM_SONG = 16;

	private static final String MESSAGE = "Message";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		HymnLyricsService service = new HymnLyricsService(
				getApplicationContext());

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			hymnId = extras.getInt("id");
			title = extras.getString("title");
			author = extras.getString("author");
			lyrics = extras.getString("lyrics");
			favorite = extras.getBoolean("favorite");
		}

		setContentView(R.layout.single_hymn);

		hymn = service.retrieveById(hymnId);

		view = this.getWindow().getDecorView();
		view.setBackgroundColor(Colors
				.getBackgroundColor(HymnLyricsPlusCoreActivity.style));

		setTitle();
		setLyrics();
	}

	private void setTitle() {
		String titleDisplay = title;
		if (favorite) {
			titleDisplay += " " + "\u2605";
		}

		TextView titleTextView = (TextView) findViewById(R.id.title);
		titleTextView.setText(Html.fromHtml("<b>" + titleDisplay + "</b>"));
		float fontSize = HymnLyricsPlusCoreActivity.fontSizeTitle
				* HymnLyricsPlusCoreActivity.scaleFactor;
		titleTextView.setTextSize(fontSize);
		titleTextView.setTextColor(Colors
				.getTextColor(HymnLyricsPlusCoreActivity.style));
	}

	private void setLyrics() {

		TextView lyricsTextView = (TextView) findViewById(R.id.lyrics);

		String text = "";
		if (author != null && author.length() > 0) {
			text += author + "\n\n";
		}

		if (HymnLyricsPlusCoreActivity.refrainBetweenStanzas) {
			lyrics = StanzaUtil.refrainBetweenAllStanzas(lyrics);
		}

		if (HymnLyricsPlusCoreActivity.showChords) {
			text += ChordUtil.showChords(lyrics);
		} else {
			text += ChordUtil.stripChords(lyrics);
		}

		lyricsTextView.setText(text);
		float fontSize = HymnLyricsPlusCoreActivity.fontSizeText
				* HymnLyricsPlusCoreActivity.scaleFactor;
		if (HymnLyricsPlusCoreActivity.showChords) {
			fontSize *= .90;
			lyricsTextView.setTypeface(Typeface.MONOSPACE);
		} else {
			lyricsTextView.setTypeface(Typeface.SANS_SERIF);
		}
		lyricsTextView.setTextSize(fontSize);
		lyricsTextView.setTextColor(Colors
				.getLyricsColor(HymnLyricsPlusCoreActivity.style));
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		menu.clear();

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.single_hymn_menu, menu);

		MenuItem addFavoriteItem = menu.findItem(R.id.add_favorite);
		MenuItem removeFavoriteItem = menu.findItem(R.id.remove_favorite);
		MenuItem shareItem = menu.findItem(R.id.share);
		MenuItem editSong = menu.findItem(R.id.edit_song);
		MenuItem deleteSong = menu.findItem(R.id.delete_song);

		if (hymn.isUserSong()) {
			shareItem.setVisible(true);
			editSong.setVisible(true);
			deleteSong.setVisible(true);
		} else {
			shareItem.setVisible(true);
			editSong.setVisible(false);
			deleteSong.setVisible(false);
		}

		if (favorite) {
			addFavoriteItem.setVisible(false);
			removeFavoriteItem.setVisible(true);
		} else {
			addFavoriteItem.setVisible(true);
			removeFavoriteItem.setVisible(false);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		int itemId = item.getItemId();
		if (itemId == R.id.share) {
			shareHymn(title, author, lyrics);
			return true;
		} else if (itemId == R.id.youtube) {
			HymnLyricsPlusCoreActivity.startVideo(title);
			return true;
		} else if (itemId == R.id.add_favorite) {
			HymnLyricsPlusCoreActivity.setFavoriteFlag(hymnId, favorite);
			favorite = true;
			setTitle();
			view.invalidate();
			return true;
		} else if (itemId == R.id.remove_favorite) {
			HymnLyricsPlusCoreActivity.setFavoriteFlag(hymnId, favorite);
			favorite = false;
			setTitle();
			view.invalidate();
			return true;
		} else if (itemId == R.id.edit_song) {
			showDialog(DIALOG_CUSTOM_SONG);
			return true;
		} else if (itemId == R.id.delete_song) {
			HymnLyricsService service = new HymnLyricsService(
					getApplicationContext());
			service.delete(hymnId);
			Toast.makeText(getApplicationContext(), "Deleted custom song",
					Toast.LENGTH_LONG).show();
			finish();
			return true;
		} else if (itemId == R.id.close) {
			finish();
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	public void shareHymn(String title, String author, String lyrics) {

		try {
			Intent shareIntent = HymnLyricsPlusCoreActivity.createShareIntent(
					title, author, lyrics);
			Intent chooser = Intent.createChooser(shareIntent, title);
			chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			startActivity(chooser);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "Could not share hymn", Toast.LENGTH_LONG)
					.show();
		}

	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
		super.onPrepareDialog(id, dialog);

		switch (id) {

		case DIALOG_MESSAGE:
			String m = bundle.getString(MESSAGE);
			((AlertDialog) dialog).setMessage(m);
			break;

		case DIALOG_CUSTOM_SONG:
			if (HymnLyricsPlusCoreActivity.ENABLE_CUSTOM_SONGS) {

				HymnLyricsService service = new HymnLyricsService(
						getApplicationContext());

				EditText title = (EditText) dialog
						.findViewById(R.id.titleInput);
				EditText author = (EditText) dialog
						.findViewById(R.id.authorInput);
				EditText lyrics = (EditText) dialog
						.findViewById(R.id.lyricsInput);

				title.setText("");
				author.setText("");
				lyrics.setText("");

				HymnLyricsMO hymnMO = service.retrieveById(hymnId);
				if (hymnMO != null) {
					title.setText(hymnMO.getTitle());
					author.setText(hymnMO.getAuthor());
					lyrics.setText(hymnMO.getLyrics());
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {
		String m = null;
		AlertDialog alert = null;
		AlertDialog.Builder builder = null;
		switch (id) {
		case DIALOG_MESSAGE:
			builder = new AlertDialog.Builder(this);
			m = bundle.getString(MESSAGE);
			builder.setMessage(m)
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			alert = builder.create();
			return alert;
		case DIALOG_CUSTOM_SONG:
			if (HymnLyricsPlusCoreActivity.ENABLE_CUSTOM_SONGS) {
				return createCustomSongDialog(hymnId);
			}
			return null;

		default:
			return null;
		}
	}

	public Dialog createCustomSongDialog(final int songId) {
		final Dialog dialog = new Dialog(this);
		HymnLyricsService service = new HymnLyricsService(this);

		dialog.setContentView(R.layout.add_edit_song);
		dialog.setTitle(getString(R.string.Custom_Song));
		dialog.setCancelable(false);

		dialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);

		final EditText title = (EditText) dialog.findViewById(R.id.titleInput);
		final EditText author = (EditText) dialog
				.findViewById(R.id.authorInput);
		final EditText lyrics = (EditText) dialog
				.findViewById(R.id.lyricsInput);

		if (songId > 0) {
			HymnLyricsMO hymnMO = service.retrieveById(songId);
			if (hymnMO != null) {
				title.setText(hymnMO.getTitle());
				author.setText(hymnMO.getAuthor());
				lyrics.setText(hymnMO.getLyrics());
			}
		}

		Button save = (Button) dialog.findViewById(R.id.song_save);

		save.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Bundle bundle = new Bundle();
				if (title.getText().toString().equals("")) {
					bundle.putString(MESSAGE, "Please enter a title");
					showDialog(DIALOG_MESSAGE, bundle);
				} else if (lyrics.getText().toString().equals("")) {
					bundle.putString(MESSAGE, "Please enter the lyrics");
					showDialog(DIALOG_MESSAGE, bundle);
				} else {
					HymnLyricsService service = new HymnLyricsService(
							getApplicationContext());
					HymnLyricsMO hymn = new HymnLyricsMO();
					hymn.setTitle(title.getText().toString());
					hymn.setAuthor(author.getText().toString());
					hymn.setLyrics(lyrics.getText().toString());
					hymn.setUserSong(true);

					hymn.setId(songId);
					service.update(hymn);

					Toast.makeText(getApplicationContext(),
							"Updated custom song", Toast.LENGTH_LONG).show();

					SingleHymnActivity.this.title = title.getText().toString();
					SingleHymnActivity.this.author = author.getText()
							.toString();
					SingleHymnActivity.this.lyrics = lyrics.getText()
							.toString();

					dismissDialog(DIALOG_CUSTOM_SONG);

					setTitle();
					setLyrics();
				}
			}
		});

		Button cancel = (Button) dialog.findViewById(R.id.song_cancel);

		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismissDialog(DIALOG_CUSTOM_SONG);
			}
		});
		return dialog;
	}
}
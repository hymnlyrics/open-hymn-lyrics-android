package com.ecodia.android.hymnlyrics.openhymnlyrics.data.dao;

import java.util.ArrayList;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.FavoritesMO;

public class FavoritesDAO {

	public static final String TABLE_NAME = "FAVORITES";

	public static final String ID = "ID";
	public static final String TITLE = "TITLE";

	public static void createTable(SQLiteDatabase db) {

		String sql = "CREATE TABLE " + TABLE_NAME + "(" + ID
				+ " INTEGER PRIMARY KEY, " + TITLE + " TEXT);";
		db.execSQL(sql);
	}

	public static void dropTable(SQLiteDatabase db) {

		String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
		db.execSQL(sql);
	}

	public static long create(SQLiteDatabase db, FavoritesMO favorites) {
		ContentValues cv = new ContentValues();
		cv.put(TITLE, favorites.getTitle());

		long insert = db.insert(TABLE_NAME, ID, cv);
		return insert;
	}

	public static int update(SQLiteDatabase db, FavoritesMO favorites) {
		ContentValues cv = new ContentValues();
		cv.put(TITLE, favorites.getTitle());
		int update = db.update(TABLE_NAME, cv, "ID=?", new String[] { ""
				+ favorites.getId() });
		return update;
	}

	public static void delete(SQLiteDatabase db, long rowID) {
		db.delete(TABLE_NAME, ID + "=" + rowID, null);
	}

	public static void deleteAll(SQLiteDatabase db) {
		db.delete(TABLE_NAME, null, null);
	}

	public static ArrayList<FavoritesMO> retrieveAll(SQLiteDatabase db) {
		ArrayList<FavoritesMO> data = retrieve(db, null, null);
		return data;
	}
	
	private static ArrayList<FavoritesMO> retrieve(SQLiteDatabase db,
			String selection, String[] selectionArgs) {
		ArrayList<FavoritesMO> data = new ArrayList<FavoritesMO>();

		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = getColumns();
		String groupBy = null;
		String having = null;
		String orderBy = TITLE;
		cursor = db.query(TABLE_NAME, columns, selection, selectionArgs,
				groupBy, having, orderBy);

		cursor.moveToFirst();

		if (!cursor.isAfterLast()) {
			do {
				FavoritesMO FavoritesMO = convertToObject(cursor);
				data.add(FavoritesMO);
			} while (cursor.moveToNext());
		}

		cursor.close();

		return data;
	}

	private static FavoritesMO convertToObject(Cursor cursor) {
		FavoritesMO favorites = new FavoritesMO();
		int i = 0;
		favorites.setId(cursor.getInt(i++));
		favorites.setTitle(cursor.getString(i++));

		return favorites;
	}

	private static String[] getColumns() {
		String cols[] = { ID, TITLE};
		return cols;
	}

}

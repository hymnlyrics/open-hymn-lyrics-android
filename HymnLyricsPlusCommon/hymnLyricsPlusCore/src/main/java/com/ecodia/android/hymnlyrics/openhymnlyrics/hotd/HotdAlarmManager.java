package com.ecodia.android.hymnlyrics.openhymnlyrics.hotd;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.ecodia.android.hymnlyrics.openhymnlyrics.R;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class HotdAlarmManager {

	private static final String TAG = HotdAlarmManager.class
			.getCanonicalName();

	Context context;

	private static PendingIntent sender;

	public HotdAlarmManager(Context context) {
		this.context = context;
	}

	public String setupAlarm() {
		Calendar cal = Calendar.getInstance();

		// Midnight next day
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.add(Calendar.DATE, 1);

		// For debugging
//		cal.set(Calendar.SECOND, 0);
//		cal.set(Calendar.MINUTE, 28);
//		cal.set(Calendar.HOUR_OF_DAY, 17);

		SimpleDateFormat df = new SimpleDateFormat();
		String sdate = df.format(cal.getTime());
		
		Log.d(TAG, "Hymn of the day notification set to " + sdate);
		
		Intent intent = new Intent(HotdAlarmReceiver.HOTD_ALARM);
		sender = PendingIntent.getBroadcast(context, R.string.app_name, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		
		 am.setRepeating(AlarmManager.RTC,
		 cal.getTimeInMillis(),
		 AlarmManager.INTERVAL_DAY, sender);
		
		 return sdate;
}

	public void cancel() {
		try {
			AlarmManager am = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			am.cancel(sender);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onDestroy() {
	}

}

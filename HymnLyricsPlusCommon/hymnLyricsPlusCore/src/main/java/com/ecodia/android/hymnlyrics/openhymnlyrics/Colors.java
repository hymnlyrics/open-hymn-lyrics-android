package com.ecodia.android.hymnlyrics.openhymnlyrics;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;


public class Colors {
	public static final int WHITE_BLACK = 0;
	public static final int GRAY_BLACK = 1;
	public static final int WHITE_GRAY = 2;
	public static final int BLACK_WHITE = 3;
	public static final int BLACK_GRAY = 4;
	public static final int BLUE_WHITE = 5;
	public static final int BLUE_BLACK = 6;
	public static final int GREEN_GRAY = 7;
	public static final int GREEN_BLACK = 8;
	public static final int PINK_WHITE = 9;
	public static final int PINK_BLACK = 10;
	public static final int MAGENTA_WHITE = 11;
	public static final int MAGENTA_BLACK = 12;
	
	public static int getTextColor(int style) {
		int color = Color.WHITE;
		switch (style) {
		case WHITE_BLACK:
			color = Color.WHITE;
			break;
		case GRAY_BLACK:
			color = Color.parseColor("#b5b5b5");
			break;
		case WHITE_GRAY:
			color = Color.WHITE;
			break;
		case BLACK_WHITE:
			color = Color.BLACK;
			break;
		case BLACK_GRAY:
			color = Color.BLACK;
			break;
		case BLUE_WHITE:
			color = Color.BLUE;
			break;
		case BLUE_BLACK:
			color = Color.BLUE;
			break;
		case GREEN_GRAY:
			color = Color.GREEN;
			break;
		case GREEN_BLACK:
			color = Color.GREEN;
			break;
		case PINK_WHITE:
			color = Color.parseColor("#ff69b4");
			break;
		case PINK_BLACK:
			color = Color.parseColor("#ff69b4");
			break;
		case MAGENTA_WHITE:
			color = Color.MAGENTA;
			break;
		case MAGENTA_BLACK:
			color = Color.MAGENTA;
			break;
		}
		return color;
	}

	public static int getBackgroundColor(int style) {
		int color = Color.BLACK;
		switch (style) {
		case WHITE_BLACK:
			color = Color.BLACK;
			break;
		case GRAY_BLACK:
			color = Color.BLACK;
			break;
		case WHITE_GRAY:
			color = Color.DKGRAY;
			break;
		case BLACK_WHITE:
			color = Color.WHITE;
			break;
		case BLUE_WHITE:
			color = Color.WHITE;
			break;
		case BLUE_BLACK:
			color = Color.BLACK;
			break;
		case BLACK_GRAY:
			color = Color.LTGRAY;
			break;
		case GREEN_GRAY:
			color = Color.GRAY;
			break;
		case GREEN_BLACK:
			color = Color.BLACK;
			break;
		case PINK_WHITE:
			color = Color.WHITE;
			break;
		case PINK_BLACK:
			color = Color.BLACK;
			break;
		case MAGENTA_WHITE:
			color = Color.WHITE;
			break;
		case MAGENTA_BLACK:
			color = Color.BLACK;
			break;
		}
		return color;
	}

	
	public static Drawable getDivider(int style) {
		Drawable color = new ColorDrawable(0x000000);
		int[] colors = new int[3];
		switch (style) {
		case WHITE_BLACK:
			colors[0] = Color.BLACK;
			colors[1] = Color.WHITE;
			colors[2] = Color.BLACK;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
		case GRAY_BLACK:
			colors[0] = Color.BLACK;
			colors[1] = Color.parseColor("#f3f3f3");
			colors[2] = Color.BLACK;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
		case WHITE_GRAY:
			colors[0] = Color.DKGRAY;
			colors[1] = Color.WHITE;
			colors[2] = Color.DKGRAY;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case BLACK_WHITE:
			colors[0] = Color.WHITE;
			colors[1] = Color.BLACK;
			colors[2] = Color.WHITE;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case BLUE_WHITE:
			colors[0] = Color.WHITE;
			colors[1] = Color.BLUE;
			colors[2] = Color.WHITE;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case BLACK_GRAY:
			colors[0] = Color.LTGRAY;
			colors[1] = Color.BLACK;
			colors[2] = Color.LTGRAY;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case BLUE_BLACK:
			break;
		case GREEN_GRAY:
			colors[0] = Color.GRAY;
			colors[1] = Color.GREEN;
			colors[2] = Color.GRAY;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case GREEN_BLACK:
			break;
		case PINK_WHITE:
			colors[0] = Color.WHITE;
			colors[1] = Color.RED;
			colors[2] = Color.WHITE;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case PINK_BLACK:
			colors[0] = Color.BLACK;
			colors[1] = Color.RED;
			colors[2] = Color.BLACK;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case MAGENTA_WHITE:
			colors[0] = Color.WHITE;
			colors[1] = Color.MAGENTA;
			colors[2] = Color.WHITE;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		case MAGENTA_BLACK:
			colors[0] = Color.BLACK;
			colors[1] = Color.MAGENTA;
			colors[2] = Color.BLACK;
			color = new GradientDrawable(Orientation.RIGHT_LEFT, colors);
			break;
		default:
		}
		return color;
	}
	public static int getLyricsColor(int style) {
		return getTextColor(style);
	}
}

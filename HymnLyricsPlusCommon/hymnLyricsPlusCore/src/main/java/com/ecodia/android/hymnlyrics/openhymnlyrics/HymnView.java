package com.ecodia.android.hymnlyrics.openhymnlyrics;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

class HymnView extends LinearLayout {
	private final HymnListAdapter hymnListAdapter;

	public HymnView(HymnListAdapter hymnListAdapter, Context context,
			String title, String dialogue, boolean hasChords, boolean expanded) {
		super(context);
		this.hymnListAdapter = hymnListAdapter;

		this.setOrientation(VERTICAL);

		View topSpacer = new View(context);
		topSpacer.setMinimumHeight(15);

		addView(topSpacer);

		mTitle = new TextView(context);
		setTitle(title);
		addView(mTitle);

		mLyrics = new TextView(context);
		setLyrics(dialogue, hasChords);
		addView(mLyrics);

		mLyrics.setVisibility(expanded ? VISIBLE : GONE);

		View bottomSpacer = new View(context);
		bottomSpacer.setMinimumHeight(15);

		addView(bottomSpacer);
	}

	public void setTitle(String title) {
		mTitle.setText(Html.fromHtml("<b>" + title + "</b>"));
		float fontSize = HymnLyricsPlusCoreActivity.fontSizeTitle
		* this.hymnListAdapter.hymnLyricsActivity.scaleFactor;
		mTitle.setTextSize(fontSize);
		mTitle.setTypeface(HymnLyricsPlusCoreActivity.typeface);
		mTitle.setTextColor(Colors.getTextColor(HymnLyricsPlusCoreActivity.style));
	}

	public void setLyrics(String words, boolean hasChords) {
		mLyrics.setText(words);
		float fontSize = HymnLyricsPlusCoreActivity.fontSizeText
		* HymnLyricsPlusCoreActivity.scaleFactor;
		if (HymnLyricsPlusCoreActivity.showChords && hasChords) {
			fontSize *= .90;
			mLyrics.setTypeface(Typeface.MONOSPACE);
		}
		else {
			mLyrics.setTypeface(Typeface.SANS_SERIF);
		}
		mLyrics.setTextSize(fontSize);
		mLyrics.setTextColor(Colors.getLyricsColor(HymnLyricsPlusCoreActivity.style));
	}

	public void setExpanded(boolean expanded) {
		mLyrics.setVisibility(expanded ? VISIBLE : GONE);
	}

	private TextView mTitle;
	private TextView mLyrics;
	
}
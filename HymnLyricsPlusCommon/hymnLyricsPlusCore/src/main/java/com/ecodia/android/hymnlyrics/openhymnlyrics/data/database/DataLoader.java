package com.ecodia.android.hymnlyrics.openhymnlyrics.data.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.util.Log;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.HymnDataSet;
import com.ecodia.android.hymnlyrics.openhymnlyrics.HymnLyricsPlusCoreActivity;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;

public class DataLoader {

	private static final String TAG = DataLoader.class.getCanonicalName();

	public static ArrayList<HymnLyricsMO> getAllInsertDataSets() throws Exception {

		if (HymnLyricsPlusCoreActivity.hymnLyricsData == null) {
			throw new Exception("HymnLyricsData is null");
		}

		HymnDataSet data = HymnLyricsPlusCoreActivity.hymnLyricsData
				.getInsertDataSet();

		ArrayList<HymnLyricsMO> hymns = DataLoader.convert(
				data.getTitlesList(), data.getTuneList(),
				data.getLastUpdateList(), data.getAuthorList(),
				data.getKeywordList(), data.getLyricsList(),
				data.getBackgroundList());

		return hymns;
	}

	public static ArrayList<HymnLyricsMO> getRecentInsertDataSets(Date hymnbookLastUpdate) throws Exception {

		ArrayList<HymnLyricsMO> hymnsAll = getAllInsertDataSets();

		ArrayList<HymnLyricsMO> hymns = new ArrayList<HymnLyricsMO>();

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		for (HymnLyricsMO hymnLyricsMO : hymnsAll) {
			try {
				Date songLastUpdate = null;
				if (hymnLyricsMO.getLastUpdate().length() > 0) {
					songLastUpdate = format.parse(hymnLyricsMO.getLastUpdate());
				}
				if (hymnbookLastUpdate == null || (songLastUpdate != null && hymnbookLastUpdate.before(songLastUpdate))) {
					hymns.add(hymnLyricsMO);
				}
			} catch (ParseException e) {
				Log.e(TAG, "Could not parse date for " + hymnLyricsMO.getTitle() + " : " + hymnLyricsMO.getLastUpdate());
			}
		}

		return hymns;
	}

	private static ArrayList<HymnLyricsMO> convert(
			ArrayList<String> titlesList, ArrayList<String> tuneList,
			ArrayList<String> lastUpdateList, ArrayList<String> authorList,
			ArrayList<String> keywordList, ArrayList<String> lyricsList,
			ArrayList<String> backgroundList) {

		ArrayList<HymnLyricsMO> set = new ArrayList<HymnLyricsMO>();

		int count = titlesList.size();

		if (count != authorList.size() || count != lyricsList.size()
				|| count != tuneList.size() || count != lastUpdateList.size()
				|| count != backgroundList.size()
				|| count != keywordList.size()) {
			Log.e(TAG, "Lyrics data sizes do not match");
			Log.e(TAG, "Title: " + titlesList.size());
			Log.e(TAG, "Tune: " + tuneList.size());
			Log.e(TAG, "Last update: " + lastUpdateList.size());
			Log.e(TAG, "Author: " + authorList.size());
			Log.e(TAG, "Keyword: " + keywordList.size());
			Log.e(TAG, "Lyrics: " + lyricsList.size());
			Log.e(TAG, "Background: " + backgroundList.size());
		}

		for (int i = 0; i < count; i++) {
			HymnLyricsMO hymnLyrics = new HymnLyricsMO();
			hymnLyrics.setTitle(titlesList.get(i));
			hymnLyrics.setTune(tuneList.get(i));
			hymnLyrics.setLastUpdate(lastUpdateList.get(i));
			hymnLyrics.setAuthor(authorList.get(i));
			hymnLyrics.setKeywords(keywordList.get(i));
			hymnLyrics.setLyrics(lyricsList.get(i));
			hymnLyrics.setBackground(backgroundList.get(i));
			set.add(hymnLyrics);
		}

		return set;
	}

}

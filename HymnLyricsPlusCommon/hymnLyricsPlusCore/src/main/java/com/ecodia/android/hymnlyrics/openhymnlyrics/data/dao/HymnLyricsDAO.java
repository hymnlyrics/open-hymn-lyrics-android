package com.ecodia.android.hymnlyrics.openhymnlyrics.data.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.ecodia.android.hymnlyrics.openhymnlyrics.Sort;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;

public class HymnLyricsDAO {

	private static final String TAG = HymnLyricsDAO.class.getCanonicalName();

	public static final String TABLE_NAME = "HYMN_LYRICS";

	public static final String ID = "ID";
	public static final String TITLE = "TITLE";
	public static final String AUTHOR = "AUTHOR";
	public static final String YEAR = "YEAR";
	public static final String LYRICS = "LYRICS";
	public static final String BACKGROUND = "BACKGROUND";
	public static final String KEYWORDS = "KEYWORDS";
	public static final String FAVORITE = "FAVORITE";
	public static final String HAS_CHORDS = "HAS_CHORDS";
	public static final String USER_SONG = "USER_SONG";
	public static final String LAST_UPDATE = "TEXT_1";
	public static final String TUNE = "TUNE";
	public static final String TEXT_2 = "TEXT_2";
	public static final String TEXT_3 = "TEXT_3";
	public static final String TEXT_4 = "TEXT_4";
	public static final String TEXT_5 = "TEXT_5";
	public static final String HYMN_NUMBER = "INT_1";
	public static final String INT_2 = "INT_2";
	public static final String INT_3 = "INT_3";

	public static void createTable(SQLiteDatabase db) {

		String sql = "CREATE TABLE " + TABLE_NAME + "(" + ID
				+ " INTEGER PRIMARY KEY, " + TITLE + " TEXT, " + AUTHOR
				+ " TEXT, " + YEAR + " TEXT, " + LYRICS + " TEXT, "
				+ HAS_CHORDS + " INTEGER," + USER_SONG + " INTEGER,"
				+ BACKGROUND + " TEXT, " + KEYWORDS + " TEXT, " + FAVORITE
				+ " INTEGER, " + LAST_UPDATE + " TEXT," + TUNE + " TEXT,"
				+ TEXT_2 + " TEXT," + TEXT_3 + " TEXT," + TEXT_4 + " TEXT,"
				+ TEXT_5 + " TEXT," + HYMN_NUMBER + " INTEGER," + INT_2 + " INTEGER,"
				+ INT_3 + " INTEGER" + ");";
		db.execSQL(sql);
	}

	public static void dropTable(SQLiteDatabase db) {

		String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
		db.execSQL(sql);
	}

	public static boolean checkColumnExists(SQLiteDatabase db, String table,
			String column) {
		try {
			String sql = "SELECT " + column + " FROM " + table;
			db.execSQL(sql);
			return true;

		} catch (Exception e) {
			Log.d(TAG, e.getLocalizedMessage());
			return false;
		}
	}

	public static void addColumn(SQLiteDatabase db, String column, String type) {

		String sql = "ALTER TABLE " + TABLE_NAME + " ADD COLUMN " + column
				+ " " + type;
		db.execSQL(sql);
	}

	public static long create(SQLiteDatabase db, HymnLyricsMO hymnLyrics) {
		checkIfHasChords(hymnLyrics);

		ContentValues cv = new ContentValues();
		cv.put(TITLE, hymnLyrics.getTitle());
		cv.put(AUTHOR, hymnLyrics.getAuthor());
		cv.put(YEAR, hymnLyrics.getYear());
		cv.put(LYRICS, hymnLyrics.getLyrics());
		cv.put(BACKGROUND, hymnLyrics.getBackground());
		String keywords = hymnLyrics.getKeywords();
		if (keywords != null) {
			keywords = keywords.trim();
			if (!keywords.endsWith(",")) {
				keywords += ",";
			}
		}
		cv.put(KEYWORDS, keywords);
		if (hymnLyrics.isFavorite()) {
			cv.put(FAVORITE, 1);
		} else {
			cv.put(FAVORITE, 0);
		}
		if (hymnLyrics.hasChords()) {
			cv.put(HAS_CHORDS, 1);
		} else {
			cv.put(HAS_CHORDS, 0);
		}
		if (hymnLyrics.isUserSong()) {
			cv.put(USER_SONG, 1);
		} else {
			cv.put(USER_SONG, 0);
		}
		cv.put(LAST_UPDATE, hymnLyrics.getLastUpdate());
		cv.put(TUNE, hymnLyrics.getTune());
		if (hymnLyrics.getHymnNumber() == null || hymnLyrics.getHymnNumber().intValue() == 0) {
			cv.put(HYMN_NUMBER, new Integer(Sort.MAX));
		} else {
			cv.put(HYMN_NUMBER, hymnLyrics.getHymnNumber());
		}
		
		long insert = db.insert(TABLE_NAME, ID, cv);
		return insert;
	}

	public static int update(SQLiteDatabase db, HymnLyricsMO hymnLyrics) {
		ContentValues cv = new ContentValues();
		cv.put(TITLE, hymnLyrics.getTitle());
		cv.put(AUTHOR, hymnLyrics.getAuthor());
		cv.put(YEAR, hymnLyrics.getYear());
		cv.put(LYRICS, hymnLyrics.getLyrics());
		cv.put(BACKGROUND, hymnLyrics.getBackground());
		String keywords = hymnLyrics.getKeywords();
		if (keywords != null) {
			keywords = keywords.trim();
			if (!keywords.endsWith(",")) {
				keywords += ",";
			}
		}
		cv.put(KEYWORDS, keywords);
		if (hymnLyrics.isFavorite()) {
			cv.put(FAVORITE, 1);
		} else {
			cv.put(FAVORITE, 0);
		}
		if (hymnLyrics.hasChords()) {
			cv.put(HAS_CHORDS, 1);
		} else {
			cv.put(HAS_CHORDS, 0);
		}
		if (hymnLyrics.isUserSong()) {
			cv.put(USER_SONG, 1);
		} else {
			cv.put(USER_SONG, 0);
		}
		cv.put(LAST_UPDATE, hymnLyrics.getLastUpdate());
		cv.put(TUNE, hymnLyrics.getTune());
		cv.put(HYMN_NUMBER, hymnLyrics.getHymnNumber());

		int update = db.update(TABLE_NAME, cv, "ID=?", new String[] { ""
				+ hymnLyrics.getId() });
		return update;
	}

	public static void delete(SQLiteDatabase db, long rowID) {
		db.delete(TABLE_NAME, ID + "=" + rowID, null);
	}

	public static void deleteAllExceptCustom(SQLiteDatabase db) {
		db.delete(TABLE_NAME, "" + USER_SONG + "<>?", new String[] { "1" });
	}

	public static void deleteCustom(SQLiteDatabase db) {
		db.delete(TABLE_NAME, "" + USER_SONG + "=?", new String[] { "1" });
	}

	public static void deleteEverything(SQLiteDatabase db) {
		db.delete(TABLE_NAME, null, null);
	}

	public static void deleteAll(SQLiteDatabase db) {
		db.delete(TABLE_NAME, null, null);
	}

	public static ArrayList<HymnLyricsMO> retrieveAll(SQLiteDatabase db, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, null, null, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveAllByTitle(SQLiteDatabase db,
			String text, int sort) {

		String[] args = new String[] { "%" + text + "%" };

		if (text != null && text.length() < 2) {
			args = new String[] { text + "%" };
		}

		ArrayList<HymnLyricsMO> data = retrieve(db, "" + TITLE + " like ?",
				args, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveAllByKeyword(
			SQLiteDatabase db, String text, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + KEYWORDS + " like ?",
				new String[] { "%" + text + ",%" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveAllByTitleAndLyrics(
			SQLiteDatabase db, String title, String lyrics, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + TITLE + " like ? or "
				+ LYRICS + " like ?", new String[] { "%" + title + "%",
				"%" + lyrics + "%" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveAllByLyrics(
			SQLiteDatabase db, String text, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + LYRICS + " like ?",
				new String[] { "%" + text + "%" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveAllByAuthor(
			SQLiteDatabase db, String text, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + AUTHOR + " like ?",
				new String[] { "%" + text + "%" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveAllByTune(
			SQLiteDatabase db, String text, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + TUNE + " like ?",
				new String[] { "%" + text + "%" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveFavorites(SQLiteDatabase db, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + FAVORITE + "=?",
				new String[] { "1" }, sort);
		return data;
	}

	public static int clearFavorites(SQLiteDatabase db) {
		ContentValues cv = new ContentValues();
		cv.put(FAVORITE, "0");
		int update = db.update(TABLE_NAME, cv, null, null);
		return update;
	}

	public static int clearNumbers(SQLiteDatabase db) {
		ContentValues cv = new ContentValues();
		cv.put(HYMN_NUMBER, new Integer(Sort.MAX));
		int update = db.update(TABLE_NAME, cv, null, null);
		return update;
	}

	public static HymnLyricsMO retrieveById(SQLiteDatabase db, int id) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + ID + "=?",
				new String[] { "" + id });
		if (data != null && data.get(0) != null) {
			return data.get(0);
		} else {
			return null;
		}
	}

	public static HymnLyricsMO retrieveByTitle(SQLiteDatabase db, String title) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + TITLE + "=?",
				new String[] { title });
		if ((data != null) && (data.size() > 0) && (data.get(0) != null)) {
			return data.get(0);
		} else {
			return null;
		}
	}

	public static ArrayList<HymnLyricsMO> retrieveChords(SQLiteDatabase db, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + HAS_CHORDS + "=?",
				new String[] { "1" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrievePopularWithNoChords(
			SQLiteDatabase db, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + HAS_CHORDS
				+ "<>? and " + KEYWORDS + " like ?", new String[] { "1",
				"%Popular,%" }, sort);
		return data;
	}

	public static ArrayList<HymnLyricsMO> retrieveCustomSongs(SQLiteDatabase db, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + USER_SONG + "=?",
				new String[] { "1" }, sort);
		return data;
	}

	public static HymnLyricsMO retrieveByTitleExcludeCustom(SQLiteDatabase db,
			String title) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + TITLE + "=? and "
				+ USER_SONG + "=?", new String[] { title, "0" });
		if (data != null && data.size() > 0) {
			return data.get(0);
		} else {
			return null;
		}
	}

	public static ArrayList<HymnLyricsMO> retrieveBuiltinSongs(SQLiteDatabase db, int sort) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + USER_SONG + "=?",
				new String[] { "0" }, sort);
		return data;
	}

	public static HymnLyricsMO retrieveCustomSongByTitle(SQLiteDatabase db,
			String title) {
		ArrayList<HymnLyricsMO> data = retrieve(db, "" + TITLE + "=? and "
				+ USER_SONG + "=?", new String[] { title, "1" });
		if (data != null && data.size() > 0) {
			return data.get(0);
		} else {
			return null;
		}
	}

	private static ArrayList<HymnLyricsMO> retrieve(SQLiteDatabase db,
			String selection, String[] selectionArgs) {

		return retrieve(db, selection, selectionArgs, Sort.TITLE);
	}
	
	private static ArrayList<HymnLyricsMO> retrieve(SQLiteDatabase db,
			String selection, String[] selectionArgs, int sort) {
		ArrayList<HymnLyricsMO> data = new ArrayList<HymnLyricsMO>();

		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = getColumns();
		String groupBy = null;
		String having = null;
		String orderBy = TITLE;
		if (sort == Sort.HYMN_NUMBER) {
			orderBy = HYMN_NUMBER + "," + TITLE;
		}
		cursor = db.query(TABLE_NAME, columns, selection, selectionArgs,
				groupBy, having, orderBy);

		cursor.moveToFirst();

		if (!cursor.isAfterLast()) {
			do {
				HymnLyricsMO HymnLyricsMO = convertToObject(cursor);
				data.add(HymnLyricsMO);
			} while (cursor.moveToNext());
		}

		cursor.close();

		return data;
	}

	public static int getCount(SQLiteDatabase db) {
		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = { "count(*)" };
		String groupBy = null;
		String having = null;
		String orderBy = null;
		cursor = db.query(TABLE_NAME, columns, null, null, groupBy, having,
				orderBy);

		cursor.moveToFirst();

		int count = cursor.getInt(0);

		cursor.close();

		return count;
	}

	public static int getCountWithoutCustom(SQLiteDatabase db) {
		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = { "count(*)" };
		String groupBy = null;
		String having = null;
		String orderBy = null;
		String selection = USER_SONG + "=?";
		String[] selectionArgs = new String[] { "0" };
		cursor = db.query(TABLE_NAME, columns, selection, selectionArgs,
				groupBy, having, orderBy);

		cursor.moveToFirst();

		int count = cursor.getInt(0);

		cursor.close();

		return count;
	}

	public static int getCustomCount(SQLiteDatabase db) {
		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = { "count(*)" };
		String groupBy = null;
		String having = null;
		String orderBy = null;
		String selection = USER_SONG + "=?";
		String[] selectionArgs = new String[] { "1" };
		cursor = db.query(TABLE_NAME, columns, selection, selectionArgs,
				groupBy, having, orderBy);

		cursor.moveToFirst();

		int count = cursor.getInt(0);

		cursor.close();

		return count;
	}

	public static int getMinId(SQLiteDatabase db) {
		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = { "min(" + ID + ")" };
		String groupBy = null;
		String having = null;
		String orderBy = null;
		cursor = db.query(TABLE_NAME, columns, null, null, groupBy, having,
				orderBy);

		cursor.moveToFirst();

		int i = cursor.getInt(0);

		cursor.close();

		return i;
	}

	public static int getMaxId(SQLiteDatabase db) {
		Cursor cursor;

		// ask the database object to create the cursor.
		String[] columns = { "max(" + ID + ")" };
		String groupBy = null;
		String having = null;
		String orderBy = null;
		cursor = db.query(TABLE_NAME, columns, null, null, groupBy, having,
				orderBy);

		cursor.moveToFirst();

		int i = cursor.getInt(0);

		cursor.close();

		return i;
	}

	private static HymnLyricsMO convertToObject(Cursor cursor) {
		HymnLyricsMO hymnLyrics = new HymnLyricsMO();
		int i = 0;
		hymnLyrics.setId(cursor.getInt(i++));
		hymnLyrics.setTitle(cursor.getString(i++));
		hymnLyrics.setAuthor(cursor.getString(i++));
		hymnLyrics.setYear(cursor.getString(i++));
		hymnLyrics.setLyrics(cursor.getString(i++));
		hymnLyrics.setBackground(cursor.getString(i++));
		hymnLyrics.setKeywords(cursor.getString(i++));
		if (cursor.getInt(i++) == 0) {
			hymnLyrics.setFavorite(false);
		} else {
			hymnLyrics.setFavorite(true);
		}

		if (cursor.getInt(i++) == 0) {
			hymnLyrics.setHasChords(false);
		} else {
			hymnLyrics.setHasChords(true);
		}

		if (cursor.getInt(i++) == 0) {
			hymnLyrics.setUserSong(false);
		} else {
			hymnLyrics.setUserSong(true);
		}
		hymnLyrics.setLastUpdate(cursor.getString(i++));
		hymnLyrics.setTune(cursor.getString(i++));
		hymnLyrics.setHymnNumber(cursor.getInt(i++));

		return hymnLyrics;
	}

	private static void checkIfHasChords(HymnLyricsMO hymnLyrics) {
		String lyrics = hymnLyrics.getLyrics();
		if (lyrics.contains("<") && lyrics.contains(">")) {
			hymnLyrics.setHasChords(true);
		} else {
			hymnLyrics.setHasChords(false);
		}
	}

	private static String[] getColumns() {
		String cols[] = { ID, TITLE, AUTHOR, YEAR, LYRICS, BACKGROUND,
				KEYWORDS, FAVORITE, HAS_CHORDS, USER_SONG, LAST_UPDATE, TUNE,
				HYMN_NUMBER};
		return cols;
	}

	public static Date getLatestUpdateDate(SQLiteDatabase db) {

		if (getCount(db) == 0) {
			return null;
		}

		String strDate = DatabaseUtils.stringForQuery(db, "SELECT DISTINCT(LAST_UPDATE) FROM " + TABLE_NAME + " ORDER BY LAST_UPDATE DESC", null);

		Date date = null;

		if (strDate.length() > 4) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try {
				date = format.parse(strDate);
			} catch (ParseException e) {
				Log.e(TAG, "Date format error: " + strDate);
			}
		}
		return date;
	}

	public static String[] getAllKeywords(SQLiteDatabase db) {

		TreeSet<String> cats = new TreeSet<String>();

		Cursor cursor;

		String[] columns = {KEYWORDS};
		cursor = db.query(TABLE_NAME, columns, null, null, null, null, null);

		cursor.moveToFirst();

		if (!cursor.isAfterLast()) {
			do {
				String keywords = cursor.getString(0);
				String[] category = keywords.split(",");
				for (int x = 0; x < category.length; x++) {
					String cat = category[x].trim();
					if (cat.length() > 0) {
						cats.add(cat);
					}
				}

			} while (cursor.moveToNext());
		}

		cursor.close();

		ArrayList<String> list = new ArrayList<String>();
		Iterator<String> cat = cats.iterator();
		while (cat.hasNext()) {
			list.add(cat.next());
		}
		return list.toArray(new String[list.size()]);
	}

}

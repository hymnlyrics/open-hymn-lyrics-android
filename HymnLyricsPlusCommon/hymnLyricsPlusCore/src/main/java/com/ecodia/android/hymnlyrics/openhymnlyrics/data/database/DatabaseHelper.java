package com.ecodia.android.hymnlyrics.openhymnlyrics.data.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ecodia.android.hymnlyrics.openhymnlyrics.data.dao.FavoritesDAO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.dao.HymnLyricsDAO;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static final String TAG = DatabaseHelper.class.getCanonicalName();

	public static String DATABASE = "HymnLyricsPlus";
	private static final int VERSION = 3;

	public DatabaseHelper(Context ctx) {
		super(ctx, DATABASE, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.v(TAG, "Creating the database");
		HymnLyricsDAO.createTable(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.v(TAG, "Upgrading the database");
		
		FavoritesDAO.dropTable(db);
		FavoritesDAO.createTable(db);
		
		upgradeDatabase(db);
	}

	private void upgradeDatabase(SQLiteDatabase db) {

		String columns[][] = { { HymnLyricsDAO.TUNE, "TEXT" },
				{ HymnLyricsDAO.TEXT_4, "TEXT" },
				{ HymnLyricsDAO.TEXT_5, "TEXT" } };

		for (String[] col : columns) {
			boolean exists = HymnLyricsDAO.checkColumnExists(db,
					HymnLyricsDAO.TABLE_NAME, col[0]);
			if (!exists) {
				Log.v(TAG, "Adding column " + col[0]);
				try {
					HymnLyricsDAO.addColumn(db, col[0], col[1]);
				} catch (Exception e) {
					Log.e(TAG, "Could not add " + col[0]);
					Log.e(TAG, e.getLocalizedMessage());
				}
			}
		}
	}
}

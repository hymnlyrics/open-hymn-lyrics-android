package com.ecodia.android.hymnlyrics.openhymnlyrics;

import java.util.ArrayList;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.LyricsUtil;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.service.HymnLyricsService;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

class HymnListAdapter extends BaseAdapter {

	final HymnLyricsPlusCoreActivity hymnLyricsActivity;
	private Context mContext;

	private Boolean[] mExpanded;
	private ArrayList<HymnLyricsMO> hymnLyrics = new ArrayList<HymnLyricsMO>();

	public HymnListAdapter(HymnLyricsPlusCoreActivity hymnLyricssActivity,
			Context context) {
		this.hymnLyricsActivity = hymnLyricssActivity;
		mContext = context;
	}

	public void reloadData(int sort) {
		if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_ALL) {
			retrieveAll(sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_FAVORITES) {
			retrieveFavorites(sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_CHORDS) {
			retrieveChords(sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_POPULAR_WITH_NO_CHORDS) {
			retrievePopularWithNoChords(sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_CUSTOM_SONGS) {
			retrieveCustomSongs(sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_BUILTIN_SONGS) {
			retrieveBuiltinSongs(sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_TITLE) {
			retrieveAllByTitle(HymnLyricsPlusCoreActivity.filterText, sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_LYRICS) {
			retrieveAllByLyrics(HymnLyricsPlusCoreActivity.filterText, sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_AUTHOR) {
			retrieveAllByAuthor(HymnLyricsPlusCoreActivity.filterText, sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_CATEGORY) {
			retrieveAllByKeyword(HymnLyricsPlusCoreActivity.filterText, sort);
		} else if (HymnLyricsPlusCoreActivity.filterMode == HymnLyricsPlusCoreActivity.FILTER_MODE_TITLE_LYRICS) {
			retrieveAllByTitleAndLyrics(HymnLyricsPlusCoreActivity.filterText,
					HymnLyricsPlusCoreActivity.filterText, sort);
		} else {
			retrieveAll(sort);	
		}
		notifyDataSetChanged();
	}

	public void retrieveAll(int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveAll(sort));
		
		collapseHymns();
	}

	public void retrieveAllByTitle(String text, int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveAllByTitle(text, sort));

		collapseHymns();
	}

	public void retrieveAllByKeyword(String text, int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveAllByKeyword(text, sort));

		collapseHymns();
	}

	public void retrieveAllByTitleAndLyrics(String title, String lyrics, int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveByTitleAndLyrics(title, lyrics, sort));

		collapseHymns();
	}

	public void retrieveAllByLyrics(String text, int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveAllByLyrics(text, sort));

		collapseHymns();
	}

	public void retrieveAllByAuthor(String text, int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveAllByAuthor(text, sort));

		collapseHymns();
	}

	public void retrieveAllByTune(String text, int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveAllByTune(text, sort));

		collapseHymns();
	}

	public void retrieveFavorites(int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveFavorites(sort));

		collapseHymns();
	}

	public void retrieveChords(int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveChords(sort));

		collapseHymns();
	}

	public void retrievePopularWithNoChords(int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrievePopularWithNoChords(sort));

		collapseHymns();
	}

	public void retrieveCustomSongs(int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveCustomSongs(sort));

		collapseHymns();
	}

	public void retrieveBuiltinSongs(int sort) {
		HymnLyricsService service = new HymnLyricsService(mContext);
		hymnLyrics.clear();
		hymnLyrics.addAll(service.retrieveBuiltinSongs(sort));

		collapseHymns();
	}

	public void collapseHymns() {
		if (hymnLyrics != null) {
			mExpanded = new Boolean[hymnLyrics.size()];
			for (int i = 0; i < hymnLyrics.size(); i++) {
				mExpanded[i] = false;
			}
		}
		hymnLyricsActivity.hideKeyboard();
	}

	public HymnLyricsMO getHymn(int position) {
		return hymnLyrics.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		HymnView sv;
		HymnLyricsMO hymn = getHymn(position);

		String text = LyricsUtil.buildLyrics(hymn.getAuthor(), hymn.getKeywords(),
				hymn.getLyrics(), mContext.getString(R.string.Tune),
				hymn.getTune(), HymnLyricsPlusCoreActivity.showChords, HymnLyricsPlusCoreActivity.refrainBetweenStanzas);

		String title = hymn.getTitle();

		if (hymn.isFavorite()) {
			title += " " + "\u2605";
		}

		if (HymnLyricsPlusCoreActivity.showHymnNumbers) {
			if (hymn.getHymnNumber() != null && hymn.getHymnNumber() != 0 && hymn.getHymnNumber() != 9999999) {
				title = hymn.getHymnNumber() + " - " + title;
			}
		}
		
		if (convertView == null) {
			sv = new HymnView(this, mContext, title, text, hymn.hasChords(), mExpanded[position]);
		} else {
			sv = (HymnView) convertView;
			sv.setTitle(title);
			sv.setLyrics(text, hymn.hasChords());
			sv.setExpanded(mExpanded[position]);
		}

		return sv;
	}

	public void toggle(int position) {
		mExpanded[position] = !mExpanded[position];
		notifyDataSetChanged();
		hymnLyricsActivity.hideKeyboard();
	}

	public Object getItem(int arg0) {
		return null;
	}

	public long getItemId(int arg0) {
		return 0;
	}

	public int getCount() {

		if (hymnLyrics != null) {
			return hymnLyrics.size();
		} else {
			return 0;
		}
	}
	
	public void scrollTo(String title) {
		int count = 0;
		boolean found = false;
		boolean searchFirstLetter = false;
		boolean searchNumber = false;
		boolean searchContains = false;
		
		if (title == null || title.length() == 0) return;
		
		if (Character.isDigit(title.charAt(0))) {
			searchNumber = true;
		}
		else if (title.charAt(0) == '*') {
			searchContains = true;
			title = title.substring(1);
		}
		else {
			searchFirstLetter = true;
		}
		
		for (HymnLyricsMO hymn : hymnLyrics) {
			if (searchNumber) {
				if (hymn.getHymnNumber().toString().startsWith(title.toLowerCase())) {
					found = true;
					break;
				}
			}
			else if (searchContains) {
				if (hymn.getTitle().toLowerCase().contains(title.toLowerCase())) {
					found = true;
					break;
				}
			}
			else if (searchFirstLetter) {
				if (hymn.getTitle().toLowerCase().startsWith(title.toLowerCase())) {
					found = true;
					break;
				}
			}
			count++;
		}
		if (found) {
			hymnLyricsActivity.lv.setSelection(count);
		}
	}
}
package com.ecodia.android.hymnlyrics.openhymnlyrics.data.service;

import java.util.ArrayList;
import java.util.Date;

import com.ecodia.android.hymnlyrics.openhymnlyrics.HymnLyricsPlusCoreActivity;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.dao.HymnLyricsDAO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.database.DataLoader;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.database.DatabaseManager;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class HymnLyricsService {

	private static final String TAG = HymnLyricsService.class.getCanonicalName();

	SQLiteDatabase db = null;
	Context context;

	public HymnLyricsService(Context context) {

		try {
			this.context = context;
			DatabaseManager instance = DatabaseManager.getInstance(context);
			this.db = instance.getDb();
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}

	private void loadDataIntoDatabase() throws Exception {
		ArrayList<HymnLyricsMO> set = DataLoader.getAllInsertDataSets();
		for (HymnLyricsMO hymn : set) {
			create(hymn);
		}
	}
	
	public void reloadDataIntoDatabase() throws Exception {
		ArrayList<HymnLyricsMO> set = DataLoader.getAllInsertDataSets();
		for (HymnLyricsMO hymn : set) {
			createOrUpdateByTitle(hymn);
		}
	}
	
	private void createOrUpdateByTitle(HymnLyricsMO hymn) {
		HymnLyricsMO existing = retrieveByTitle(hymn.getTitle());
		if (existing == null) {
			create(hymn);
		}
		else {
			existing.setAuthor(hymn.getAuthor());
			existing.setTune(hymn.getTune());
			existing.setBackground(hymn.getBackground());
			existing.setKeywords(hymn.getKeywords());
			existing.setLyrics(hymn.getLyrics());
			existing.setTitle(hymn.getTitle());
			existing.setYear(hymn.getYear());
			update(existing);
		}
	}

	public long create(HymnLyricsMO hymnLyricsMO) {
		try {
			createSortTitle(hymnLyricsMO);
			return HymnLyricsDAO.create(db, hymnLyricsMO);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	public long update(HymnLyricsMO hymnLyricsMO) {
		try {
			createSortTitle(hymnLyricsMO);
			return HymnLyricsDAO.update(db, hymnLyricsMO);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	public void delete(long rowID) {
		try {
			HymnLyricsDAO.delete(db, rowID);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteAll() {
		try {
			HymnLyricsDAO.deleteAll(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteAllExceptCustom() {
		try {
			HymnLyricsDAO.deleteAllExceptCustom(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteEverything() {
		try {
			HymnLyricsDAO.deleteEverything(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteCustom() {
		try {
			HymnLyricsDAO.deleteCustom(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public void deleteBuiltin() {
		try {
			HymnLyricsDAO.deleteCustom(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
	}

	public HymnLyricsMO retrieveById(int id) {
		try {
			return HymnLyricsDAO.retrieveById(db, id);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public HymnLyricsMO retrieveByTitleExcludeCustom(String title) {
		try {
			return HymnLyricsDAO.retrieveByTitleExcludeCustom(db, title);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public HymnLyricsMO retrieveCustomSongByTitle(String title) {
		try {
			return HymnLyricsDAO.retrieveCustomSongByTitle(db, title);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveChords(int sort) {
		try {
			return HymnLyricsDAO.retrieveChords(db, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrievePopularWithNoChords(int sort) {
		try {
			return HymnLyricsDAO.retrievePopularWithNoChords(db, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveCustomSongs(int sort) {
		try {
			return HymnLyricsDAO.retrieveCustomSongs(db, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveBuiltinSongs(int sort) {
		try {
			return HymnLyricsDAO.retrieveBuiltinSongs(db, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	
	public HymnLyricsMO retrieveByTitle(String title) {
		try {
			return HymnLyricsDAO.retrieveByTitle(db, title);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveAll(int sort) {
		try {
			return HymnLyricsDAO.retrieveAll(db, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveAllByTitle(String text, int sort) {
		try {
			return HymnLyricsDAO.retrieveAllByTitle(db, text, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveAllByKeyword(String text, int sort) {
		try {
			return HymnLyricsDAO.retrieveAllByKeyword(db, text, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveByTitleAndLyrics(String title, String lyrics, int sort) {
		try {
			return HymnLyricsDAO.retrieveAllByTitleAndLyrics(db, title, lyrics, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveAllByLyrics(String text, int sort) {
		try {
			return HymnLyricsDAO.retrieveAllByLyrics(db, text, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public ArrayList<HymnLyricsMO> retrieveAllByAuthor(String text, int sort) {
		try {
			return HymnLyricsDAO.retrieveAllByAuthor(db, text, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<HymnLyricsMO> retrieveAllByTune(String text, int sort) {
		try {
			return HymnLyricsDAO.retrieveAllByTune(db, text, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<HymnLyricsMO> retrieveFavorites(int sort) {
		try {
			return HymnLyricsDAO.retrieveFavorites(db, sort);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	public int clearFavorites() {
		try {
			return HymnLyricsDAO.clearFavorites(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}
	
	public int clearNumbers() {
		try {
			return HymnLyricsDAO.clearNumbers(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getCount() {
		try {
			return HymnLyricsDAO.getCount(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	public void setAsFavorites(ArrayList<HymnLyricsMO> favorites) {
		for (HymnLyricsMO fav : favorites) {
			HymnLyricsMO hymn = retrieveByTitle(fav.getTitle());
			hymn.setFavorite(true);
			update(hymn);
		}	
	}


	public int getCountWithoutCustom() {
		try {
			return HymnLyricsDAO.getCountWithoutCustom(db);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getCustomCount() {
		try {
			return HymnLyricsDAO.getCustomCount(db);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getMinId() {
		try {
			return HymnLyricsDAO.getMinId(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}

	public int getMaxId() {
		try {
			return HymnLyricsDAO.getMaxId(db);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
			e.printStackTrace();
		}
		return -1;
	}
	
	private void createSortTitle(HymnLyricsMO hymnLyricsMO) {
		String sortTitle = hymnLyricsMO.getTitle();
		sortTitle = sortTitle.toLowerCase();
		String firstChar = sortTitle.substring(0, 1);
		if (HymnLyricsPlusCoreActivity.languageCode == null) {
		}
		else if (HymnLyricsPlusCoreActivity.languageCode.equals("es")) {
			if (firstChar.equals("¡") || firstChar.equals("¿")) {
				sortTitle = sortTitle.substring(1);
			}
			firstChar = sortTitle.substring(0, 1);
			if (firstChar.equals("á")) {
				sortTitle = "a~" + sortTitle.substring(1);
			}
		}
		else if (HymnLyricsPlusCoreActivity.languageCode.equals("ha")) {
			if (firstChar.equals("�") || firstChar.equals("�")) {
				sortTitle = sortTitle.substring(1);
			}
			else if (firstChar.equals("�")) {
				sortTitle = "a" + sortTitle.substring(1);
			}
		}
		else if (HymnLyricsPlusCoreActivity.languageCode.equals("ro")) {
			StringBuilder sb = new StringBuilder();
			for (int i=0; i< sortTitle.length(); i++) {
				char c = sortTitle.charAt(i);
				// ăãâîşșţț
				if (c == 'ă' || c == 'ã' || c == 'â') {
					sb.append("a~");
				}
				else if (c == 'î') {
					sb.append("i~");
				}
				else if (c == 'ş' || c == 'ș') {
					sb.append("s~");
				}
				else if (c == 'ţ' || c == 'ț') {
					sb.append("t~");
				}
				else {
					sb.append(c);
				}
			}
			sortTitle = sb.toString();
		}
		hymnLyricsMO.setSortTitle(sortTitle);
	}

	public Date setLatestUpdateDate() {
		return HymnLyricsDAO.getLatestUpdateDate(db);
	}

	public String[] getAllKeywords() {
		return HymnLyricsDAO.getAllKeywords(db);
	}
}

package com.ecodia.android.hymnlyrics.openhymnlyrics.hotd;

import com.ecodia.android.hymnlyrics.openhymnlyrics.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * This is an activity that provides an interstitial UI for the notification
 * that is posted by {@link IncomingMessage}. It allows the user to switch to
 * the app in its appropriate state if they want.
 */
public class IncomingMessageInterstitial extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.notification_message);

		Button button = (Button) findViewById(R.id.notify_app);
		button.setOnClickListener(new Button.OnClickListener() {
			public void onClick(View v) {
				switchToApp();
			}
		});
	}

	void switchToApp() {
		int id = getIntent().getIntExtra("id", 1);
		// Intent[] stack = HymnOfTheDay.makeMessageIntentStack(this, id);
		// startActivities(stack);
		Intent intent = HymnOfTheDay.makeMessageIntent(this, id);
		startActivity(intent);
		finish();
	}
}
package com.ecodia.android.hymnlyrics.openhymnlyrics.hotd;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class HotdService extends Service {
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		new HymnOfTheDay(getApplicationContext()).showAppNotification();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}
package com.ecodia.android.hymnlyrics.openhymnlyrics;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.ClipboardManager;
import android.content.ClipData;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ecodia.android.hymnlyrics.hymnlyricslibrary.ChordUtil;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.IHymnLyricsData;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.ResourceUtil;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.StanzaUtil;
import com.ecodia.android.hymnlyrics.hymnlyricslibrary.StringUtil;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.database.DataLoader;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.model.HymnLyricsMO;
import com.ecodia.android.hymnlyrics.openhymnlyrics.data.service.HymnLyricsService;
import com.ecodia.android.hymnlyrics.openhymnlyrics.hotd.HotdAlarmManager;
import com.ecodia.android.hymnlyrics.openhymnlyrics.hotd.HotdAlarmReceiver;

public class HymnLyricsPlusCoreActivity extends ListActivity implements
        OnItemLongClickListener {

    private static final String TAG = HymnLyricsPlusCoreActivity.class
            .getCanonicalName();

    private static HymnLyricsPlusCoreActivity mActivity;

    public static boolean ENABLE_CUSTOM_SONGS = true;

    public static IHymnLyricsData hymnLyricsData;
    public static String fontName;
    public static String version;
    public static String contributersText;
    public static String aboutText;
    public static String supportText;
    public static String privacyText;
    public static String languageCode;
    public static Locale locale;
    public static float fontSizeTitle = 18;
    public static float fontSizeText = 16;
    public static String packageName = "com.ecodia.android.openhymnlyrics";

    public static Typeface typeface;
    private static Context context;

    private static final int DIALOG_ABOUT = 10;
    private static final int DIALOG_SETTINGS = 11;
    private static final int DIALOG_FILTER = 12;
    private static final int DIALOG_CATEGORY = 13;
    private static final int DIALOG_RELOAD_DATA = 14;
    private static final int DIALOG_HYMN_INFO = 15;
    private static final int DIALOG_MESSAGE = 9;
    private static final int DIALOG_CUSTOM_SONG = 16;
    private static final int DIALOG_DELETE_CUSTOM = 17;
    private static final int DIALOG_DELETE_BUILTIN = 18;
    private static final int DIALOG_HYMN_NUMBER = 19;

    private static final int DIALOG_LVL_GOTOMARKET = 90;
    private static final int DIALOG_LVL_RETRY = 91;

    private static final String MESSAGE = "Message";

    private static final String PREFERENCES_FILE = "HymnLyricsPlus";
    private static final String SCALE_FACTOR = "scaleFactor";
    private static final String FONT_SETTING_POS = "fontSettingPosition";
    private static final String YOUTUBE_SEARCH = "youtubeSearch";
    //	private static final String KEEP_SCREEN_ON = "keepScreenOn";
    private static final String OPEN_HYMN_IN_NEW_SCREEN = "openHymnInNewScreen";
    private static final String STYLE_SETTING_POS = "styleSettingPosition";
    private static final String SHOW_CHORDS = "showChords";
    private static final String REFRAIN_BETWEEN_STANZAS = "refrainBetweenStanzas";
    private static final String HYMN_OF_THE_DAY = "hymnOfTheDay";
    private static final String VALID_LICENSE = "check";
    private static final String SORT_BY = "sortBy";
    private static final String SHOW_HYMN_NUMBER = "showHymnNumber";
    private static final String LAUNCHER_EXTRA = "launcher";
    private static final String LANGUAGE_EXTRA = "language";
    private static final String FILTER_TEXT = "filterText";

    private static final int DEFAULT_FONT_SETTING_POS = 2;
    private static final int DEFAULT_STYLE_SETTING_POS = 0;

    public static int filterMode;
    public static String filterText;

    public static final int FILTER_MODE_ALL = 0;
    public static final int FILTER_MODE_FAVORITES = 1;
    public static final int FILTER_MODE_TITLE = 2;
    public static final int FILTER_MODE_LYRICS = 3;
    public static final int FILTER_MODE_TITLE_LYRICS = 4;
    public static final int FILTER_MODE_CATEGORY = 5;
    public static final int FILTER_MODE_AUTHOR = 6;
    public static final int FILTER_MODE_CHORDS = 7;
    public static final int FILTER_MODE_POPULAR_WITH_NO_CHORDS = 8;
    public static final int FILTER_MODE_CUSTOM_SONGS = 9;
    public static final int FILTER_MODE_BUILTIN_SONGS = 10;

    ScaleGestureDetector scaleGestureDetector;

    static float scaleFactor = 1.f;
    //	static boolean keepScreenOn = false;
    static boolean openHymnInNewScreen = false;
    static String youtubeSearch = "";

    static boolean showChords = false;
    static boolean notifyHotd = false;
    static int style = 0;
    static boolean refrainBetweenStanzas = false;
    static String validLicense = "";
    static int sortBy;
    static boolean showHymnNumbers = false;

    private static ProgressDialog progressDialog;

    public static HymnListAdapter hymnListAdapter;
    public ListView lv;
    private int listOffset;

    private ArrayAdapter<CharSequence> spinnerAdapter;
    private Spinner spinner;
    //	private CheckBox keepScreenOnCheckBox;
    private CheckBox openHymnInNewScreenCheckBox;

    private LinearLayout topLayout;
    private CheckBox showChordsCheckBox;
    private CheckBox refrainBetweenStanzasCheckBox;
    private CheckBox notifyHotdCheckBox;
    private CheckBox showHymnNumbersCheckBox;
    private EditText youtubeEditText;

    private Spinner fontSizeSpinner;
    private int spinnerFontPosition;
    private String spinnerFontSelection;

    private Spinner styleSpinner;

    private BroadcastReceiver receiver;

    private Handler mHandler;

    private TextView overlay;
    private float touchX;
    private float touchY;

    public static String SONGS_CUSTOM_FILE = "OpenHymnLyricsCustom.txt";
    public static String SONGS_NUMBERS_FILE = "OpenHymnLyricsNumbers.txt";
    public static String SONGS_FAVORITES_FILE = "OpenHymnLyricsFavorites.txt";
    public static String SONGS_TITLE_FILE = "OpenHymnLyricsTitles.txt";

    private int songId = -1;

    boolean mExternalStorageAvailable = false;
    boolean mExternalStorageWriteable = false;
    String storageState = Environment.getExternalStorageState();

    private static final int ACTIVITY_CHOOSE_LOAD_FILE = 1;
    private static final int ACTIVITY_CHOOSE_SAVE_FILE = 2;

    private static final int REQUEST_PERMISSIONS = 100;
    private static final String PERMISSIONS_REQUIRED[] = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.INTERNET,
    };

    public static String launcherExtra;
    public static String languageExtra;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(TAG, "Lifecycle: onCreate");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String launcher = extras.getString("launcher", null);
            if (launcher != null) {
                launcherExtra = launcher;
            }
            String language = extras.getString("language", null);
            if (language != null) {
                languageExtra = language;
            }
        }

        mActivity = this;

        context = getApplicationContext();

        readInstanceState(this);

        if (hymnLyricsData == null) {
            Intent intent = null;
            try {
                Log.i(TAG, "Relaunching activity " + launcherExtra);
                intent = new Intent(this, Class.forName(launcherExtra));
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            finish();
        }

        setLocale();

        this.overlay = (TextView) View.inflate(this, R.layout.overlay, null);

        getWindowManager()
                .addView(
                        overlay,
                        new WindowManager.LayoutParams(
                                LayoutParams.WRAP_CONTENT,
                                LayoutParams.WRAP_CONTENT,
                                WindowManager.LayoutParams.TYPE_APPLICATION,
                                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                        | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                PixelFormat.TRANSLUCENT));

        checkPermissions();

        initializeSongData();

        int viewId = ResourceUtil.getIdByName(getPackageName(), "layout",
                "hymns");
        setContentView(viewId);

        topLayout = (LinearLayout) findViewById(R.id.toplayout);

        try {
            if (fontName != null) {
                typeface = Typeface.createFromAsset(getAssets(), "fonts/"
                        + fontName);
                Log.i(TAG, "Loaded font " + fontName);
            } else {
                Log.i(TAG, "Using default font");
                typeface = Typeface.DEFAULT;
            }
        } catch (Exception e) {
            Log.i(TAG, "Could not load font " + fontName);
            e.printStackTrace();
        }

        if (notifyHotd) {
            new HotdAlarmManager(getApplicationContext()).setupAlarm();

            registerAlarm();
        }

//		if (keepScreenOn) {
//			getWindow()
//					.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//			getWindow().getDecorView().setKeepScreenOn(keepScreenOn);
//		} else {
//			getWindow().clearFlags(
//					WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//			getWindow().getDecorView().setKeepScreenOn(keepScreenOn);
//		}

        hymnListAdapter = new HymnListAdapter(this, this);
        hymnListAdapter.retrieveAll(sortBy);
        filterMode = FILTER_MODE_ALL;
        setFilterCriteriaText(FILTER_MODE_ALL, null);
        setListAdapter(hymnListAdapter);

        lv = getListView();
        lv.setOnItemLongClickListener(this);

        lv.setDivider(Colors.getDivider(style));
        lv.setDividerHeight(1);

        lv.setOnScrollListener(new OnScrollListener() {

            boolean visible;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                hideKeyboard();
                if (scrollState == ListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    int fromRight = lv.getWidth() - (int) touchX;

                    if (fromRight > 100) {
                        visible = false;
                    } else {
                        visible = true;
                    }
                } else if (scrollState == ListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    overlay.setVisibility(View.INVISIBLE);
                    visible = false;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (visible) {
                    HymnLyricsMO hymn = hymnListAdapter
                            .getHymn(firstVisibleItem);
                    String firstChar = hymn.getTitle().substring(0, 1);
                    if (firstChar.equals("¡") || firstChar.equals("¿")) {
                        firstChar = hymn.getTitle().substring(1, 1);
                    }
                    overlay.setText(firstChar);
                    overlay.setVisibility(View.VISIBLE);
                }
            }
        });

        View view = getWindow().getDecorView();
        view.setBackgroundColor(Colors.getBackgroundColor(style));

        TextView textView = (TextView) findViewById(R.id.filterCriteria);
        textView.setBackgroundColor(Colors.getBackgroundColor(style));
        textView.setTextColor(Colors.getTextColor(style));

        EditText jumpTo = (EditText) findViewById(R.id.jumpToBox);
        jumpTo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable e) {
                hymnListAdapter.scrollTo(e.toString());
            }
        });

        jumpTo.setBackgroundColor(Colors.getBackgroundColor(style));
        jumpTo.setTextColor(Colors.getTextColor(style));
        jumpTo.setHintTextColor(Colors.getTextColor(style));

        if (extras != null) {
            int id = extras.getInt("open_id");
            if (id > 0) {
                showSingleHymn(id);
            }
        }

        // scaleGestureDetector = new ScaleGestureDetector(this,
        // new OnScaleGestureListener());

        checkStorageState();
    }

    private void checkPermissions() {
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(PERMISSIONS_REQUIRED, REQUEST_PERMISSIONS);
        }
    }

    private void initializeSongData() {
        HymnLyricsService service = new HymnLyricsService(
                getApplicationContext());

        int customCount = service.getCustomCount();
        int builtinCount = service.getCountWithoutCustom();

        if (builtinCount == 0 && customCount > 0) {
            // Don't load data
            Log.d(TAG, "Do not initialize the database");
        } else if (hymnLyricsData != null) {
            Log.d(TAG, "Initialize the database");
            ArrayList<HymnLyricsMO> set = null;
            try {
                set = DataLoader.getAllInsertDataSets();
                if (service.getCountWithoutCustom() != set.size()) {
                    loadBuiltinData();
                }
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void loadBuiltinData() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMessage(getString(R.string.Initializing_data));
        progressDialog.setProgress(0);
        progressDialog.show();

        Thread builtinLoaderThread = new BuiltinLoaderThread(
                builtinLoaderHandler);
        builtinLoaderThread.start();
    }

    private Handler builtinLoaderHandler = new Handler() {
        public void handleMessage(Message msg) {
            try {
                int total = msg.arg1;

                progressDialog.setProgress(total);
                if (total == 100) {

                    filterMode = FILTER_MODE_ALL;
                    setFilterCriteriaText(FILTER_MODE_ALL, null);

                    hymnListAdapter.reloadData(sortBy);

                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            } catch (Throwable e) {
                Log.e(TAG, e.getMessage());
            }
        }
    };

    private Dialog createAboutDialog() {
        Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.about);
        dialog.setTitle(getString(R.string.About));
        dialog.setCancelable(true);

        TextView versionMsg = (TextView) dialog
                .findViewById(R.id.about_version);
        versionMsg.setText(getString(R.string.Version) + ": " + version);

        TextView forumMsg = (TextView) dialog.findViewById(R.id.about_text);
        String message = "";
        if (aboutText != null) {
            message += aboutText + "<br/>";
        }
        if (supportText != null) {
            message += supportText + "<br/>";
        }
        if (privacyText != null) {
            message += privacyText + "<br/>";
        }
        if (contributersText != null) {
            message += getString(R.string.Contributors) + ":<br/>"
                    + contributersText + "<br/>";
        }
        Spanned htmlSpan = Html.fromHtml(message, null, null);
        forumMsg.setText(htmlSpan);
        forumMsg.setMovementMethod(LinkMovementMethod.getInstance());

        Button cancel = (Button) dialog.findViewById(R.id.okButton);

        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dismissDialog(DIALOG_ABOUT);
            }
        });

        return dialog;
    }

    private Dialog createSettingDialog() {
        final Dialog settingDialog = new Dialog(this);

        settingDialog.setContentView(R.layout.settings);
        settingDialog.setTitle(getString(R.string.Settings));
        settingDialog.setCancelable(true);

        fontSizeSpinner = (Spinner) settingDialog
                .findViewById(R.id.fontSizeSpinner);
        if (fontSizeSpinner != null) {
            spinnerAdapter = ArrayAdapter.createFromResource(
                    getApplicationContext(), R.array.sizes,
                    android.R.layout.simple_spinner_dropdown_item);
            fontSizeSpinner.setAdapter(spinnerAdapter);
            fontSizeSpinner.setSelection(spinnerFontPosition);
            fontSizeSpinner
                    .setOnItemSelectedListener(new OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent,
                                                   View v, int pos, long row) {

                            HymnLyricsPlusCoreActivity.this.spinnerFontPosition = pos;
                            HymnLyricsPlusCoreActivity.this.spinnerFontSelection = parent
                                    .getItemAtPosition(pos).toString();
                        }

                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                    });
        }

        styleSpinner = (Spinner) settingDialog.findViewById(R.id.styleSpinner);
        if (styleSpinner != null) {
            spinnerAdapter = ArrayAdapter.createFromResource(
                    getApplicationContext(), R.array.styles,
                    android.R.layout.simple_spinner_dropdown_item);
            styleSpinner.setAdapter(spinnerAdapter);
            styleSpinner.setSelection(HymnLyricsPlusCoreActivity.style);
            styleSpinner
                    .setOnItemSelectedListener(new OnItemSelectedListener() {
                        public void onItemSelected(AdapterView<?> parent,
                                                   View v, int pos, long row) {

                            HymnLyricsPlusCoreActivity.style = pos;
                            // HymnLyricsPlusCoreActivity.this.spinnerStyleSelection
                            // = parent
                            // .getItemAtPosition(pos).toString();
                        }

                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                    });
        }

//		keepScreenOnCheckBox = (CheckBox) settingDialog
//				.findViewById(R.id.keepScreenOnCheckbox);
//		keepScreenOnCheckBox.setChecked(keepScreenOn);
//		keepScreenOnCheckBox
//				.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//					public void onCheckedChanged(CompoundButton buttonView,
//							boolean isChecked) {
//						if (isChecked) {
//							keepScreenOn = true;
//							getWindow()
//									.addFlags(
//											WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//							getWindow().getDecorView().setKeepScreenOn(
//									keepScreenOn);
//						} else {
//							keepScreenOn = false;
//							getWindow()
//									.clearFlags(
//											WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//							getWindow().getDecorView().setKeepScreenOn(
//									keepScreenOn);
//						}
//
//					}
//				});

        refrainBetweenStanzasCheckBox = (CheckBox) settingDialog
                .findViewById(R.id.refrainBetweenStanzasCheckBox);
        refrainBetweenStanzasCheckBox.setChecked(refrainBetweenStanzas);
        refrainBetweenStanzasCheckBox
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            refrainBetweenStanzas = true;
                        } else {
                            refrainBetweenStanzas = false;
                        }
                    }
                });

        showHymnNumbersCheckBox = (CheckBox) settingDialog
                .findViewById(R.id.showHymnNumbersCheckbox);
        showHymnNumbersCheckBox.setChecked(showHymnNumbers);
        showHymnNumbersCheckBox
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            showHymnNumbers = true;
                            sortBy = Sort.HYMN_NUMBER;
                        } else {
                            showHymnNumbers = false;
                            sortBy = Sort.TITLE;
                        }
                    }
                });

        openHymnInNewScreenCheckBox = (CheckBox) settingDialog
                .findViewById(R.id.openHymnInNewScreenCheckbox);
        openHymnInNewScreenCheckBox.setChecked(openHymnInNewScreen);
        openHymnInNewScreenCheckBox
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            openHymnInNewScreen = true;
                            ((HymnListAdapter) getListAdapter())
                                    .collapseHymns();
                            ((HymnListAdapter) getListAdapter())
                                    .notifyDataSetChanged();
                        } else {
                            openHymnInNewScreen = false;
                        }

                    }
                });

        showChordsCheckBox = (CheckBox) settingDialog
                .findViewById(R.id.showChordsCheckbox);
        showChordsCheckBox.setChecked(showChords);
        showChordsCheckBox
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            showChords = true;
                        } else {
                            showChords = false;
                        }

                    }
                });

        notifyHotdCheckBox = (CheckBox) settingDialog
                .findViewById(R.id.notifyHotdCheckbox);
        notifyHotdCheckBox.setChecked(notifyHotd);
        notifyHotdCheckBox
                .setOnCheckedChangeListener(new OnCheckedChangeListener() {
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        if (isChecked) {
                            notifyHotd = true;

                            new HotdAlarmManager(getApplicationContext())
                                    .setupAlarm();

                            registerAlarm();

                            Toast.makeText(getApplicationContext(),
                                    "Activated Hymn Of The Day",
                                    Toast.LENGTH_LONG).show();
                        } else {
                            notifyHotd = false;

                            new HotdAlarmManager(getApplicationContext())
                                    .cancel();

                            unregisterAlarm();
                            Toast.makeText(getApplicationContext(),
                                    "Deactivated Hymn Of The Day",
                                    Toast.LENGTH_LONG).show();
                        }

                    }
                });

        youtubeEditText = (EditText) settingDialog.findViewById(R.id.youtubeSearchText);
        youtubeEditText.setText(youtubeSearch);

        Button reloadButton = (Button) settingDialog
                .findViewById(R.id.reloadData);
        if (reloadButton != null) {
            reloadButton.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dismissDialog(DIALOG_SETTINGS);
                    showDialog(DIALOG_RELOAD_DATA);
                }
            });
        }

        Button deleteCustom = (Button) settingDialog
                .findViewById(R.id.deleteCustom);
        if (deleteCustom != null) {
            deleteCustom.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dismissDialog(DIALOG_SETTINGS);
                    showDialog(DIALOG_DELETE_CUSTOM);
                }
            });
        }

        Button deleteBuiltin = (Button) settingDialog
                .findViewById(R.id.deleteBuiltin);
        if (deleteBuiltin != null) {
            deleteBuiltin.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dismissDialog(DIALOG_SETTINGS);
                    showDialog(DIALOG_DELETE_BUILTIN);
                }
            });
        }

        Button button = (Button) settingDialog.findViewById(R.id.okButton);
        if (button != null) {
            button.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    dismissDialog(DIALOG_SETTINGS);
                }
            });
        }

        settingDialog.setOnDismissListener(new OnDismissListener() {

            public void onDismiss(DialogInterface dialog) {
                // String size = (String) spinner.getSelectedItem();
                String size = spinnerFontSelection;
                if (size == null) {
                    scaleFactor = 1;
                } else if (size.equals("XS")) {
                    scaleFactor = 0.70f;
                } else if (size.equals("S")) {
                    scaleFactor = 0.85f;
                } else if (size.equals("M")) {
                    scaleFactor = 1;
                } else if (size.equals("L")) {
                    scaleFactor = 1.25f;
                } else if (size.equals("XL")) {
                    scaleFactor = 1.50f;
                } else {
                    scaleFactor = 1;
                }

                TextView textView = (TextView) findViewById(R.id.filterCriteria);
                textView.setBackgroundColor(Colors.getBackgroundColor(style));
                textView.setTextColor(Colors.getTextColor(style));

                EditText jumpTo = (EditText) findViewById(R.id.jumpToBox);
                float fontSize = fontSizeText * scaleFactor;
                jumpTo.setTextSize(fontSize);
                jumpTo.setBackgroundColor(Colors.getBackgroundColor(style));
                jumpTo.setTextColor(Colors.getTextColor(style));
                jumpTo.setHintTextColor(Colors.getTextColor(style));

                View view = getWindow().getDecorView();
                view.setBackgroundColor(Colors.getBackgroundColor(style));
                if (Colors.getBackgroundColor(style) == Color.TRANSPARENT) {
                    lv.setCacheColorHint(0);
                }

                lv.setDivider(Colors.getDivider(style));
                lv.setDividerHeight(1);

                youtubeSearch = youtubeEditText.getText().toString();

                topLayout.setBackgroundColor(Colors.getBackgroundColor(style));
                hymnListAdapter.reloadData(sortBy);
            }
        });
        return settingDialog;
    }

    private String getDeviceIdHash() {
        String deviceId = getDeviceId();
        String hash = "Ex20:15." + deviceId.hashCode();
        return hash;
    }

    @SuppressLint("MissingPermission")
    private String getDeviceId() {
        final TelephonyManager tm = (TelephonyManager) getBaseContext()
                .getSystemService(Context.TELEPHONY_SERVICE);

        String tmDevice, tmSerial, androidId;

        tmDevice = "";
        tmSerial = "";
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(),
				((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = deviceUuid.toString();

		return deviceId;
	}

	private void checkStorageState() {
		if (Environment.MEDIA_MOUNTED.equals(storageState)) {
			mExternalStorageAvailable = mExternalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(storageState)) {
			mExternalStorageAvailable = true;
			mExternalStorageWriteable = false;
		} else {
			mExternalStorageAvailable = mExternalStorageWriteable = false;
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		final int action = ev.getAction();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
			touchX = ev.getX();
			touchY = ev.getY();
			break;
		case MotionEvent.ACTION_UP:
			touchX = -1;
			touchY = -1;
			break;
		default:
			break;
		}
		return super.dispatchTouchEvent(ev);
	}

	private void registerAlarm() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(HotdAlarmReceiver.HOTD_ALARM);

		receiver = new HotdAlarmReceiver();
		registerReceiver(receiver, filter);
	}

	private void unregisterAlarm() {
		if (receiver != null) {
			unregisterReceiver(receiver);
		}
	}

	public Dialog createCustomSongDialog(final int songId) {
		final Dialog dialog = new Dialog(this);
		HymnLyricsService service = new HymnLyricsService(this);

		dialog.setContentView(R.layout.add_edit_song);
		dialog.setTitle(getString(R.string.Custom_Song));
		dialog.setCancelable(false);

		dialog.getWindow().setLayout(LayoutParams.FILL_PARENT,
				LayoutParams.FILL_PARENT);

		final EditText title = (EditText) dialog.findViewById(R.id.titleInput);
		final EditText author = (EditText) dialog
				.findViewById(R.id.authorInput);
		final EditText lyrics = (EditText) dialog
				.findViewById(R.id.lyricsInput);
		final EditText hymnNumber = (EditText) dialog
				.findViewById(R.id.hymnNumberInput);
        final EditText hymnCategory = (EditText) dialog
                .findViewById(R.id.hymnCategoryInput);

		if (songId > 0) {
			HymnLyricsMO hymnMO = service.retrieveById(songId);
			if (hymnMO != null) {
				title.setText(hymnMO.getTitle());
				author.setText(hymnMO.getAuthor());
				lyrics.setText(hymnMO.getLyrics());
				hymnNumber.setText(hymnMO.getHymnNumber().toString());
                hymnCategory.setText(hymnMO.getKeywords());
			}
		}

		Button save = (Button) dialog.findViewById(R.id.song_save);

		save.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				Bundle bundle = new Bundle();
				if (title.getText().toString().equals("")) {
					bundle.putString(MESSAGE, "Please enter a title");
					showDialog(DIALOG_MESSAGE, bundle);
				} else if (lyrics.getText().toString().equals("")) {
					bundle.putString(MESSAGE, "Please enter the lyrics");
					showDialog(DIALOG_MESSAGE, bundle);
				} else {
					Integer number = new Integer(Sort.MAX);
					try {
						number = new Integer(hymnNumber.getText().toString());
					}
					catch (Exception e) {
					}
					
					HymnLyricsService service = new HymnLyricsService(
							getApplicationContext());
					HymnLyricsMO hymn = new HymnLyricsMO();
					hymn.setTitle(title.getText().toString());
					hymn.setAuthor(author.getText().toString());
					hymn.setLyrics(lyrics.getText().toString());
					hymn.setHymnNumber(number);
					hymn.setKeywords(hymnCategory.getText().toString());
					hymn.setUserSong(true);

					if (songId < 0) {
						service.create(hymn);
						dismissDialog(DIALOG_CUSTOM_SONG);
						hymnListAdapter.reloadData(sortBy);
						Toast.makeText(getApplicationContext(),
								"Added custom song", Toast.LENGTH_LONG).show();
					} else {
						hymn.setId(songId);
						service.update(hymn);
						dismissDialog(DIALOG_CUSTOM_SONG);
						hymnListAdapter.reloadData(sortBy);
						Toast.makeText(getApplicationContext(),
								"Updated custom song", Toast.LENGTH_LONG)
								.show();
					}
                    hymnLyricsData.setCategories(service.getAllKeywords());
                    hymnListAdapter.reloadData(sortBy);
				}
			}
		});

		Button cancel = (Button) dialog.findViewById(R.id.song_cancel);

		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismissDialog(DIALOG_CUSTOM_SONG);
			}
		});
		return dialog;
	}

	public Dialog createEditNumberDialog() {
		final Dialog dialog = new Dialog(this);
		HymnLyricsService service = new HymnLyricsService(this);

		dialog.setContentView(R.layout.edit_hymn_number);
		dialog.setTitle(R.string.Edit_Song_Number);
		dialog.setCancelable(false);

		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);

		final EditText hymnNumber = (EditText) dialog
				.findViewById(R.id.hymnNumberInput);
		
		Button save = (Button) dialog.findViewById(R.id.song_save);

		save.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {

				HymnLyricsService service = new HymnLyricsService(
						getApplicationContext());

				HymnLyricsMO hymnMO = service.retrieveById(songId);
				int hymnNo;
				try {
					hymnNo = Integer.parseInt(hymnNumber.getText().toString());
				} catch (Exception e) {
					hymnNo = Sort.MAX;
				}
				if (hymnNo == 0) hymnNo = Sort.MAX;
				hymnMO.setHymnNumber(new Integer(hymnNo));
				service.update(hymnMO);
				dismissDialog(DIALOG_HYMN_NUMBER);
				hymnListAdapter.reloadData(sortBy);
				Toast.makeText(getApplicationContext(),
						"Updated hymn number", Toast.LENGTH_LONG)
						.show();
			}
		});

		Button cancel = (Button) dialog.findViewById(R.id.song_cancel);

		cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dismissDialog(DIALOG_HYMN_NUMBER);
			}
		});
		return dialog;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_menu, menu);

		if (ENABLE_CUSTOM_SONGS) {
			MenuItem customSongs = (MenuItem) menu.findItem(R.id.custom_songs);

			customSongs.setVisible(true);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = null;
		Uri uri = null;
		if (item.getItemId() == R.id.about) {
			showDialog(DIALOG_ABOUT);
			return true;
		} else if (item.getItemId() == R.id.settings) {
			showDialog(DIALOG_SETTINGS);
			return true;
		} else if (item.getItemId() == R.id.custom_songs) {
			if (ENABLE_CUSTOM_SONGS) {
				showCustomSongsMenu();
			}
			return true;
		} else if (item.getItemId() == R.id.filters) {
			showSearchMenu();
			return true;
		} else if (item.getItemId() == R.id.rate_app) {
			try {
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=" + packageName));
				startActivity(intent);
				return true;
			} catch (Exception e) {
				uri = Uri.parse("http://play.google.com/store/apps/details?id="
						+ packageName);
				intent = new Intent(Intent.ACTION_VIEW, uri);
				startActivity(intent);
				return true;
			}
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		if (openHymnInNewScreen) {

			((HymnListAdapter) getListAdapter()).collapseHymns();
			((HymnListAdapter) getListAdapter()).notifyDataSetChanged();

			HymnLyricsMO hymn = ((HymnListAdapter) getListAdapter())
					.getHymn(position);

			int hymnId = hymn.getId();

			showSingleHymn(hymnId);

		} else {
			((HymnListAdapter) getListAdapter()).toggle(position);
		}
	}

	private void showSingleHymn(int hymnId) {
		HymnLyricsService service = new HymnLyricsService(
				getApplicationContext());
		HymnLyricsMO hymn = service.retrieveById(hymnId);

		Intent i = new Intent(this, SingleHymnActivity.class);
		i.putExtra("id", hymn.getId());
		i.putExtra("title", hymn.getTitle());
		i.putExtra("author", hymn.getAuthor());
		i.putExtra("lyrics", hymn.getLyrics());
		i.putExtra("favorite", hymn.isFavorite());
		startActivity(i);

	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog, Bundle bundle) {
		super.onPrepareDialog(id, dialog);

		HymnLyricsService service = new HymnLyricsService(
				getApplicationContext());

		switch (id) {
		case DIALOG_FILTER:

			switch (filterMode) {
			case FILTER_MODE_TITLE:
				dialog.setTitle(getString(R.string.Enter_title_filter));
				break;
			case FILTER_MODE_LYRICS:
				dialog.setTitle(R.string.Enter_lyrics_filter);
				break;
			case FILTER_MODE_AUTHOR:
				dialog.setTitle(R.string.Enter_author_filter);
				break;
			case FILTER_MODE_CATEGORY:
				dialog.setTitle(R.string.Select_category);
				break;
			}
			break;

		case DIALOG_SETTINGS:
//			keepScreenOnCheckBox = (CheckBox) dialog
//					.findViewById(R.id.keepScreenOnCheckbox);
//			keepScreenOnCheckBox.setChecked(keepScreenOn);

			openHymnInNewScreenCheckBox = (CheckBox) dialog
					.findViewById(R.id.openHymnInNewScreenCheckbox);
			openHymnInNewScreenCheckBox.setChecked(openHymnInNewScreen);

			showChordsCheckBox = (CheckBox) dialog
					.findViewById(R.id.showChordsCheckbox);
			showChordsCheckBox.setChecked(showChords);

			notifyHotdCheckBox = (CheckBox) dialog
					.findViewById(R.id.notifyHotdCheckbox);
			notifyHotdCheckBox.setChecked(notifyHotd);

			fontSizeSpinner = (Spinner) dialog
					.findViewById(R.id.fontSizeSpinner);
			fontSizeSpinner.setSelection(spinnerFontPosition);

			styleSpinner = (Spinner) dialog.findViewById(R.id.styleSpinner);
			styleSpinner.setSelection(style);

			break;

		case DIALOG_CUSTOM_SONG:
			if (ENABLE_CUSTOM_SONGS) {

				EditText title = (EditText) dialog
						.findViewById(R.id.titleInput);
				EditText author = (EditText) dialog
						.findViewById(R.id.authorInput);
				EditText lyrics = (EditText) dialog
						.findViewById(R.id.lyricsInput);
				EditText hymnNumber = (EditText) dialog
						.findViewById(R.id.hymnNumberInput);
				EditText category = (EditText) dialog
                        .findViewById(R.id.hymnCategoryInput);

				title.setText("");
				author.setText("");
				lyrics.setText("");
				hymnNumber.setText("");
                category.setText("");

				if (songId > 0) {
					HymnLyricsMO hymnMO = service.retrieveById(songId);
					if (hymnMO != null) {
						title.setText(hymnMO.getTitle());
						author.setText(hymnMO.getAuthor());
						lyrics.setText(hymnMO.getLyrics());
                        category.setText(hymnMO.getKeywords());
						if (hymnMO.getHymnNumber().intValue() != Sort.MAX) {
							hymnNumber.setText(hymnMO.getHymnNumber().toString());
						}
					}
				}
			}
			break;

		case DIALOG_HYMN_NUMBER:
			EditText hymnNumber = (EditText) dialog
					.findViewById(R.id.hymnNumberInput);
			hymnNumber.setText("");

			HymnLyricsMO hymnMO = service.retrieveById(songId);
			if (hymnMO != null) {
				if (hymnMO.getHymnNumber().intValue() != Sort.MAX) {
					hymnNumber.setText(hymnMO.getHymnNumber().toString());
				}
			}
				
			break;

		case DIALOG_ABOUT:
			TextView totalSongsMsg = (TextView) dialog
					.findViewById(R.id.about_total_songs);
			int count = service.getCount();
			totalSongsMsg.setText("" + count + " "
					+ getString(R.string.total_songs_in_database));

			TextView builtinSongsMsg = (TextView) dialog
					.findViewById(R.id.about_builtin_songs);
			count = service.getCountWithoutCustom();
			builtinSongsMsg.setText("" + count + " "
					+ getString(R.string.Builtin_songs));

			TextView customSongsMsg = (TextView) dialog
					.findViewById(R.id.about_custom_songs);
			count = service.getCustomCount();
			customSongsMsg.setText("" + count + " "
					+ getString(R.string.Custom_Songs));

			TextView displayedSongsMsg = (TextView) dialog
					.findViewById(R.id.about_displayed_songs);
			displayedSongsMsg.setText(hymnListAdapter.getCount() + " "
					+ getString(R.string.songs_displayed));
			break;

		case DIALOG_HYMN_INFO:
			String background = bundle.getString(MESSAGE);
			Spanned htmlSpan = Html.fromHtml(background, null, null);
			TextView backgroundView = (TextView) dialog
					.findViewById(R.id.background_text);
			backgroundView.setText(htmlSpan);
			backgroundView.setTypeface(typeface);
			backgroundView.setTextSize(fontSizeText);
			backgroundView.setMovementMethod(LinkMovementMethod.getInstance());

			break;
			
		case DIALOG_MESSAGE:
			
			String m = bundle.getString(MESSAGE);
			
			AlertDialog ad = (AlertDialog) dialog;
			ad.setMessage(m);
			
			break;

		}
	}

	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {
		final Dialog dialog = new Dialog(HymnLyricsPlusCoreActivity.this);
		String m = null;
		AlertDialog alert = null;
		AlertDialog.Builder builder = null;

		switch (id) {
		case DIALOG_LVL_GOTOMARKET:
			return new AlertDialog.Builder(this)
					.setTitle(R.string.unlicensed_dialog_title)
					.setMessage(R.string.unlicensed_dialog_body)
					.setPositiveButton(R.string.buy_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Intent marketIntent = new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("https://play.google.com/store/apps/details?id="
													+ packageName));
									startActivity(marketIntent);
								}
							})
					.setNegativeButton(R.string.quit_button,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							}).create();
		case DIALOG_LVL_RETRY:
			return new AlertDialog.Builder(this)
					.setTitle(R.string.cant_access_license_server)
					.setMessage(R.string.retry_dialog_body)
					.setPositiveButton(R.string.OK,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							}).create();
		case DIALOG_MESSAGE:
			builder = new AlertDialog.Builder(this);
			m = bundle.getString(MESSAGE);
			builder.setMessage(m)
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			alert = builder.create();
			return alert;

		case DIALOG_RELOAD_DATA:

			builder = new AlertDialog.Builder(this);
			builder.setMessage(getString(R.string.Confirm_reload_database))
					.setCancelable(true);

			builder.setPositiveButton(getString(R.string.OK),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							HymnLyricsService service = new HymnLyricsService(
									getApplicationContext());

                            loadBuiltinData();
							dialog.cancel();
						}
					});

			builder.setNegativeButton(getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							// HymnLyricsService service = new
							// HymnLyricsService(
							// getApplicationContext());
							dialog.cancel();
						}
					});

			alert = builder.create();
			return alert;

		case DIALOG_DELETE_CUSTOM:

			builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"Are you sure you want to delete all the custom songs?")
					.setCancelable(true);

			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							HymnLyricsService service = new HymnLyricsService(
									getApplicationContext());

							service.deleteCustom();
							hymnListAdapter.reloadData(sortBy);

							dialog.cancel();
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});

			alert = builder.create();
			return alert;

		case DIALOG_DELETE_BUILTIN:

			builder = new AlertDialog.Builder(this);
			builder.setMessage(
					"Are you sure you want to delete all the built-in songs?")
					.setCancelable(true);

			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {

							HymnLyricsService service = new HymnLyricsService(
									getApplicationContext());

							service.deleteAllExceptCustom();
							hymnListAdapter.reloadData(sortBy);

							dialog.cancel();
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							dialog.cancel();
						}
					});

			alert = builder.create();
			return alert;

		case DIALOG_CATEGORY:
			AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
					HymnLyricsPlusCoreActivity.this);

			final String[] list = hymnLyricsData.getCategories();

			dialogBuilder.setTitle(getString(R.string.Filter_by_category));
			dialogBuilder.setItems(list, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					String category = (String) list[item];
					hymnListAdapter.retrieveAllByKeyword(category, sortBy);
					filterMode = FILTER_MODE_CATEGORY;
					setFilterCriteriaText(FILTER_MODE_CATEGORY, category);
					hymnListAdapter.notifyDataSetChanged();
				}
			});

			dialogBuilder.setCancelable(true);
			AlertDialog catDialog = dialogBuilder.create();
			catDialog.setOnShowListener(new DialogListSetTypeFace());
			return catDialog;

		case DIALOG_FILTER:

			dialog.setContentView(R.layout.enter_filter);

			final EditText _filterView = (EditText) dialog
					.findViewById(R.id.filterInput);

			Button filter = (Button) dialog.findViewById(R.id.filter);

			filter.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					String text = _filterView.getText().toString();

					switch (filterMode) {
					case FILTER_MODE_TITLE:
						setFilterCriteriaText(FILTER_MODE_TITLE, text);
						hymnListAdapter.retrieveAllByTitle(text, sortBy);
						break;
					case FILTER_MODE_LYRICS:
						setFilterCriteriaText(FILTER_MODE_LYRICS, text);
						hymnListAdapter.retrieveAllByLyrics(text, sortBy);
						break;
					case FILTER_MODE_AUTHOR:
						setFilterCriteriaText(FILTER_MODE_AUTHOR, text);
						hymnListAdapter.retrieveAllByAuthor(text, sortBy);
						break;
					case FILTER_MODE_TITLE_LYRICS:
						setFilterCriteriaText(FILTER_MODE_LYRICS, text);
						hymnListAdapter.retrieveAllByTitleAndLyrics(text, text, sortBy);
						break;
					}

					hymnListAdapter.notifyDataSetChanged();

					dialog.dismiss();
				}
			});

			Button clear = (Button) dialog.findViewById(R.id.clear);

			clear.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {
					_filterView.setText("");
				}
			});

			// Button cancel = (Button) dialog.findViewById(R.id.cancel);
			//
			// cancel.setOnClickListener(new View.OnClickListener() {
			// public void onClick(View v) {
			// dialog.dismiss();
			// }
			// });

			return dialog;

		case DIALOG_ABOUT:
			return createAboutDialog();

		case DIALOG_SETTINGS:
			return createSettingDialog();

		case DIALOG_HYMN_INFO:
			dialog.setContentView(R.layout.hymn_info);
			dialog.setTitle(getString(R.string.Hymn_info));
			dialog.setCancelable(true);
			// cancel = (Button) dialog.findViewById(R.id.okButton);
			//
			// cancel.setOnClickListener(new View.OnClickListener() {
			// public void onClick(View v) {
			// dismissDialog(DIALOG_HYMN_INFO);
			// }
			// });
			return dialog;

		case DIALOG_CUSTOM_SONG:
			if (ENABLE_CUSTOM_SONGS) {
				return createCustomSongDialog(songId);
			}
			return null;

		case DIALOG_HYMN_NUMBER:
			return createEditNumberDialog();

		default:
			return null;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if (!writeInstanceState(this)) {
			Toast.makeText(this, "Failed to write state!", Toast.LENGTH_LONG)
					.show();
		}

		listOffset = lv.getFirstVisiblePosition();
	}

	@Override
	public void onStop() {
		super.onStop();
		Log.i(TAG, "Lifecycle: onStop");
		writeInstanceState(this);
	}
		
	@Override
	public void onDestroy() {
		super.onDestroy();
        Log.i(TAG, "Lifecycle: onDestroy");
		writeInstanceState(this);

        if (this.overlay != null) {
            getWindowManager().removeView(this.overlay);
		    this.overlay = null;
        }
	}


    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "Lifecycle: onStart");
    }

    @Override
	public void onResume() {
		super.onResume();
        Log.i(TAG, "Lifecycle: onResume");

        readInstanceState(this);

        IHymnLyricsData hymnLyricsData = HymnLyricsPlusCoreActivity.hymnLyricsData;

        hymnListAdapter.reloadData(sortBy);

		lv.setSelectionFromTop(listOffset, 0);
	}

	public boolean onItemLongClick(AdapterView<?> av, View view, int position,
			long id) {
		showClickMenu(av, view, position, id);
		return false;
	}

	private void showClickMenu(AdapterView<?> av, View view,
			final int position, long id) {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
				HymnLyricsPlusCoreActivity.this);

		HymnLyricsMO hymn = hymnListAdapter.getHymn(position);
		final int hymnId = hymn.getId();
		songId = hymnId;
		
		final HymnLyricsService service = new HymnLyricsService(
				getApplicationContext());
		hymn = service.retrieveById(hymnId);

		final String title = hymn.getTitle();
		final String lyrics = hymn.getLyrics();
		final String author = hymn.getAuthor();
		final boolean favorite = hymn.isFavorite();
		final boolean custom = hymn.isUserSong();
		final String background = hymn.getBackground();

		ArrayList<String> list = new ArrayList<String>();
		if (!hymn.isUserSong()) {
			list.add(getString(R.string.Share));
		}
		list.add(getString(R.string.Copy_to_clipboard));
		list.add(getString(R.string.Youtube));
		if (favorite) {
			list.add(getString(R.string.Remove_from_favorites));
		} else {
			list.add(getString(R.string.Add_to_favorites));
		}
		if (custom) {
			list.add(getString(R.string.Edit_Custom_Song));
			list.add(getString(R.string.Delete_Custom_Song));
		} else {
			list.add(getString(R.string.Edit_Song_Number));
		}
		if (background != null && background.length() > 0) {
			list.add(getString(R.string.Hymn_info));
		}

		final CharSequence[] options = (CharSequence[]) list
				.toArray(new CharSequence[list.size()]);

		dialogBuilder.setTitle(title);
		dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals(getString(R.string.Youtube))) {
					startVideo(title);
				} else if (options[item]
						.equals(getString(R.string.Add_to_favorites))
						|| options[item]
								.equals(getString(R.string.Remove_from_favorites))) {
					setFavoriteFlag(hymnId, favorite);
					hymnListAdapter.reloadData(sortBy);

				} else if (options[item].equals(getString(R.string.Share))) {
					shareHymn(title, author, lyrics);
				} else if (options[item].equals(getString(R.string.Copy_to_clipboard))) {
					
					ClipboardManager clipboard = (ClipboardManager)
					        getSystemService(Context.CLIPBOARD_SERVICE);
					String clipText = title + "\n\n";
					if (author != null && author.length() > 0) {
						clipText += author + "\n\n";
					}
					clipText += lyrics + "\n";

					ClipData clip = ClipData.newPlainText("Hymn", clipText);
					clipboard.setPrimaryClip(clip);
					
					Toast.makeText(getApplicationContext(),
							R.string.Copied, Toast.LENGTH_LONG).show();
				} else if (options[item]
						.equals(getString(R.string.Edit_Custom_Song))) {
					songId = hymnId;
					showDialog(DIALOG_CUSTOM_SONG);
				} else if (options[item]
						.equals(getString(R.string.Edit_Song_Number))) {
					songId = hymnId;
					showDialog(DIALOG_HYMN_NUMBER);
				} else if (options[item]
						.equals(getString(R.string.Delete_Custom_Song))) {
					service.delete(hymnId);
					Toast.makeText(getApplicationContext(),
							"Deleted custom song", Toast.LENGTH_LONG).show();
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item].equals(getString(R.string.Hymn_info))) {
					Bundle bundle = new Bundle();
					bundle.putString(MESSAGE, background);
					showDialog(DIALOG_HYMN_INFO, bundle);
				}
			}
		});
		dialogBuilder.setCancelable(true);
		dialogBuilder.create().show();
	}

	@Override
	public boolean onSearchRequested() {

		showSearchMenu();
		return false;
	}

	private void showSearchMenu() {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
				HymnLyricsPlusCoreActivity.this);

		HymnLyricsService service = new HymnLyricsService(
				getApplicationContext());

		ArrayList<String> list = new ArrayList<String>();
		list.add(getString(R.string.Show_all));
		list.add(getString(R.string.Show_favorites));
		if (ENABLE_CUSTOM_SONGS) {
			if (service.getCustomCount() > 0) {
				list.add(getString(R.string.Show_custom_songs));
				list.add(getString(R.string.Show_builtin_songs));
			}
		}
		if (HymnLyricsPlusConfig.DEV_MODE) {
			list.add(getString(R.string.Show_popular_with_no_chords));
		}
		list.add(getString(R.string.Show_songs_with_chords));
		list.add(getString(R.string.Filter_by_category));
		list.add(getString(R.string.Filter_by_title));
		list.add(getString(R.string.Filter_by_author));
		list.add(getString(R.string.Filter_by_lyrics));

		final CharSequence[] options = (CharSequence[]) list
				.toArray(new CharSequence[list.size()]);

		dialogBuilder.setTitle("Filter");
		dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals(getString(R.string.Show_all))) {
					filterMode = FILTER_MODE_ALL;
					setFilterCriteriaText(FILTER_MODE_ALL, null);
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item]
						.equals(getString(R.string.Show_favorites))) {
					filterMode = FILTER_MODE_FAVORITES;
					setFilterCriteriaText(FILTER_MODE_FAVORITES, null);
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item]
						.equals(getString(R.string.Show_songs_with_chords))) {
					filterMode = FILTER_MODE_CHORDS;
					showChords = true;
					setFilterCriteriaText(FILTER_MODE_CHORDS, null);
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item]
						.equals(getString(R.string.Show_popular_with_no_chords))) {
					filterMode = FILTER_MODE_POPULAR_WITH_NO_CHORDS;
					setFilterCriteriaText(FILTER_MODE_POPULAR_WITH_NO_CHORDS,
							null);
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item]
						.equals(getString(R.string.Show_custom_songs))) {
					filterMode = FILTER_MODE_CUSTOM_SONGS;
					setFilterCriteriaText(FILTER_MODE_CUSTOM_SONGS, null);
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item]
						.equals(getString(R.string.Show_builtin_songs))) {
					filterMode = FILTER_MODE_BUILTIN_SONGS;
					setFilterCriteriaText(FILTER_MODE_BUILTIN_SONGS, null);
					hymnListAdapter.reloadData(sortBy);
				} else if (options[item]
						.equals(getString(R.string.Filter_by_title))) {
					filterMode = FILTER_MODE_TITLE;
					showDialog(DIALOG_FILTER);
				} else if (options[item]
						.equals(getString(R.string.Filter_by_lyrics))) {
					filterMode = FILTER_MODE_LYRICS;
					showDialog(DIALOG_FILTER);
				} else if (options[item]
						.equals(getString(R.string.Filter_by_author))) {
					filterMode = FILTER_MODE_AUTHOR;
					showDialog(DIALOG_FILTER);
				} else if (options[item]
						.equals(getString(R.string.Filter_by_category))) {
					filterMode = FILTER_MODE_CATEGORY;
					showDialog(DIALOG_CATEGORY);
				}

			}
		});
		dialogBuilder.setCancelable(true);
		dialogBuilder.create().show();
	}

	private class BuiltinLoaderThread extends Thread {
		Handler mHandler;

		BuiltinLoaderThread(Handler h) {
			mHandler = h;
		}

		public void run() {

			try {
				HymnLyricsService service = new HymnLyricsService(
						getApplicationContext());

				ArrayList<HymnLyricsMO> favorites = service.retrieveFavorites(sortBy);

				ArrayList<HymnLyricsMO> set = null;
				boolean deleteAll = false;

                set = DataLoader.getAllInsertDataSets();

                // Delete all after rewrite of code or doing testing
                if ((set.size() > 2025 && set.size() < 2100) || set.size() < 4) {
                    Log.i(TAG, "Resetting all hymns");
                    service.deleteAllExceptCustom();
                    deleteAll = true;
                }
                // Just update recent songs if recently installed
                else if (service.getCount() >= 1000 || service.getCount() < 10) {
					Log.i(TAG, "Loading only recent hymns");
                    Date lastUpdate = service.setLatestUpdateDate();
                    set = DataLoader.getRecentInsertDataSets(lastUpdate);
					deleteAll = false;
				} else {
					Log.i(TAG, "Loading all hymns");
					service.deleteAllExceptCustom();
					set = DataLoader.getAllInsertDataSets();
					deleteAll = true;
				}

				Log.i(TAG, "Hymns to be loaded: " + set.size());

				int total = set.size() + 10;
				int i = 0;
				for (HymnLyricsMO hymn : set) {
					if (deleteAll) {
						service.create(hymn);
					} else {
						HymnLyricsMO existing = service.retrieveByTitle(hymn
								.getTitle());
						if (existing == null) {
							service.create(hymn);
						} else {
							hymn.setId(existing.getId());
							if (!existing.isUserSong()) {
                                service.update(hymn);
                            }
						}
					}

					if ((i % 10) == 0) {
						Message msg = mHandler.obtainMessage();
						float f = (float) i / (float) total;
						int progress = (int) (f * 100);
						msg.arg1 = progress;
						mHandler.sendMessage(msg);
					}
					i++;
				}
				service.setAsFavorites(favorites);

				showChords = false;
//				keepScreenOn = false;

				Message msg = mHandler.obtainMessage();
				msg.arg1 = 100;
				mHandler.sendMessage(msg);
			} catch (Exception e) {
				Log.e(TAG, e.getLocalizedMessage());
			} finally {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
		}
	};

	private class CustomLoaderThread extends Thread {
		List<Uri> files;
		Handler mHandler;

		CustomLoaderThread(List<Uri> f, Handler handler) {
			files = f;
			mHandler = handler;
		}

		public void run() {
			Message msg = mHandler.obtainMessage();

			try {
				int insertCount = 0;
				int updateCount = 0;
				int favoriteCount = 0;
				int hymnNumberCount = 0;

				for (Uri f : files) {
					ImportResult importResult = loadCustomSong(f);
					insertCount += importResult.insertCount;
					updateCount += importResult.updateCount;
					favoriteCount += importResult.favoriteCount;
					hymnNumberCount += importResult.hymnNumberCount;
				}

                msg.obj = "Songs inserted: " + insertCount + "\n"
						+ "Songs updated: " + updateCount + "\n"
						+ "Hymn number count: " + hymnNumberCount + "\n"
						+ "Favorites: " + favoriteCount;
			} catch (Exception e) {
				Log.w("Custom Song Loader", "Error loading: " + e.getMessage());
				msg.obj = "Error loading: " + e.getMessage();
			} finally {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
			mHandler.sendMessage(msg);
		}
	};

	private void backupCustomSongs(Uri uri) {

		progressDialog = new ProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(getString(R.string.Backing_up_custom_data));
		// progressDialog.show();

		Thread customBackupThread = new CustomBackupThread(customBackupHandler, uri);
		customBackupThread.start();
	}

	private class CustomBackupThread extends Thread {
		Handler mHandler;
		Uri mUri;

		CustomBackupThread(Handler handler, Uri uri) {
		    mHandler = handler;
		    mUri = uri;
		}

		public void run() {

			Message msg = mHandler.obtainMessage();

			try {
				HymnLyricsService service = new HymnLyricsService(
						getApplicationContext());
				
				/**
				 * Save custom data file
				 */
				StringBuffer output = new StringBuffer();

				int customSongsCount = 0;
				
				ArrayList<HymnLyricsMO> hymns = service.retrieveCustomSongs(Sort.TITLE);
				for (HymnLyricsMO hymn : hymns) {
					output.append("Title: " + hymn.getTitle() + "\n");
					output.append("Author: " + hymn.getAuthor() + "\n");
					output.append("Keywords: " + hymn.getKeywords() + "\n");
					output.append("Number: " + hymn.getHymnNumber() + "\n");
					output.append("Lyrics:\n");
					output.append(hymn.getLyrics() + "\n");
					output.append("---\n");
					customSongsCount++;
				}

                ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(mUri, "w");
				FileOutputStream fos = new FileOutputStream(pfd.getFileDescriptor());
				fos.write(output.toString().getBytes());
				fos.close();

//				/**
//				 * Save hymn numbers file
//				 */
//				file = new File(path, SONGS_NUMBERS_FILE);
//
//				output = new StringBuffer();
//
//				int hymnNumberCount = 0;
//
//				hymns = service.retrieveAll(Sort.HYMN_NUMBER);
//				for (HymnLyricsMO hymn : hymns) {
//					if (hymn.getHymnNumber() != null && hymn.getHymnNumber().intValue() != 0 &&
//							hymn.getHymnNumber().intValue() != Sort.MAX) {
//						output.append("Number: " + hymn.getHymnNumber() + "\n");
//						output.append("Title: " + hymn.getTitle() + "\n");
//						output.append("---\n");
//						hymnNumberCount++;
//					}
//				}
//
//				fos = new FileOutputStream(file);
//				fos.write(output.toString().getBytes());
//				fos.close();
//
//				/**
//				 * Save favorites file
//				 */
//				file = new File(path, SONGS_FAVORITES_FILE);
//
//				output = new StringBuffer();
//
//				int favoritesCount = 0;
//
//				ArrayList<HymnLyricsMO> favorites = service.retrieveFavorites(Sort.TITLE);
//				for (HymnLyricsMO hymn : favorites) {
//					output.append("Favorite: " + hymn.getTitle() + "\n");
//					favoritesCount++;
//				}
//
//				fos = new FileOutputStream(file);
//				fos.write(output.toString().getBytes());
//				fos.close();
//
//				/**
//				 * Write song title file
//				 */
//				file = new File(path, SONGS_TITLE_FILE);
//
//				output = new StringBuffer();
//
//				int titlesCount = 0;
//
//				hymns = service.retrieveAll(Sort.TITLE);
//				for (HymnLyricsMO hymn : hymns) {
//					output.append("Title: " + hymn.getTitle() + "\n");
//					output.append("---\n");
//					titlesCount++;
//				}
//
//				fos = new FileOutputStream(file);
//				fos.write(output.toString().getBytes());
//				fos.close();

				msg.obj = "Custom songs saved: " + customSongsCount + "\n";
//						  "Hymn numbers: " + hymnNumberCount + "\n" +
//						  "Favorites: " + favoritesCount + "\n" +
//						  "Titles:" + titlesCount;
				
			} catch (Exception e) {
				Log.w("Song Backup", "Error saving: " + e.getMessage());
				msg.obj = "Error saving: " + e.getMessage();
			} finally {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
			}
			mHandler.sendMessage(msg);
		}
	};

	private Handler customBackupHandler = new Handler() {
		public void handleMessage(Message msg) {
			try {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}

				String message = (String) msg.obj;

				Bundle bundle = new Bundle();
				bundle.putString(MESSAGE, message);
				showDialog(DIALOG_MESSAGE, bundle);

				hymnListAdapter.reloadData(sortBy);
			} catch (Throwable e) {
				Log.e(TAG, e.getMessage());
			}
		}
	};

	private Handler customLoaderHandler = new Handler() {
		public void handleMessage(Message msg) {
			try {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}

				String message = (String) msg.obj;

				Bundle bundle = new Bundle();
				bundle.putString(MESSAGE, message);
				showDialog(DIALOG_MESSAGE, bundle);

				hymnListAdapter.reloadData(sortBy);
			} catch (Throwable e) {
				Log.e(TAG, e.getMessage());
			}
		}
	};

	public boolean readInstanceState(Context c) {
		SharedPreferences p = c.getSharedPreferences(PREFERENCES_FILE,
				MODE_PRIVATE);

		if (p == null) {
			return false;
		}

		spinnerFontPosition = p.getInt(FONT_SETTING_POS,
				DEFAULT_FONT_SETTING_POS);
		scaleFactor = p.getFloat(SCALE_FACTOR, 1);
//		keepScreenOn = p.getBoolean(KEEP_SCREEN_ON, false);
		openHymnInNewScreen = p.getBoolean(OPEN_HYMN_IN_NEW_SCREEN, false);
		style = p.getInt(STYLE_SETTING_POS, DEFAULT_STYLE_SETTING_POS);
		showChords = p.getBoolean(SHOW_CHORDS, false);
		refrainBetweenStanzas = p.getBoolean(REFRAIN_BETWEEN_STANZAS, false);
		notifyHotd = p.getBoolean(HYMN_OF_THE_DAY, false);
        youtubeSearch = p.getString(YOUTUBE_SEARCH, "");

        validLicense = p.getString(VALID_LICENSE, "");
		sortBy = p.getInt(SORT_BY, Sort.TITLE);
		showHymnNumbers = p.getBoolean(SHOW_HYMN_NUMBER, false);

		launcherExtra = p.getString(LAUNCHER_EXTRA, launcherExtra);
        languageExtra = p.getString(LANGUAGE_EXTRA, languageExtra);

        filterText = p.getString(FILTER_TEXT, filterText);

        return (p.contains(SCALE_FACTOR));
	}

	public boolean writeInstanceState(Context c) {

		SharedPreferences p = c.getSharedPreferences(
				HymnLyricsPlusCoreActivity.PREFERENCES_FILE, MODE_PRIVATE);

		SharedPreferences.Editor e = p.edit();

		e.putFloat(SCALE_FACTOR, HymnLyricsPlusCoreActivity.scaleFactor);
		e.putInt(FONT_SETTING_POS, this.spinnerFontPosition);
//		e.putBoolean(KEEP_SCREEN_ON, keepScreenOn);
		e.putBoolean(OPEN_HYMN_IN_NEW_SCREEN, openHymnInNewScreen);
        e.putString(YOUTUBE_SEARCH, HymnLyricsPlusCoreActivity.youtubeSearch);

        e.putInt(STYLE_SETTING_POS, style);
		e.putBoolean(SHOW_CHORDS, showChords);
		e.putBoolean(REFRAIN_BETWEEN_STANZAS, refrainBetweenStanzas);
		e.putBoolean(HYMN_OF_THE_DAY, notifyHotd);
		
		e.putString(VALID_LICENSE, validLicense);
		
		e.putInt(SORT_BY, sortBy);
		e.putBoolean(SHOW_HYMN_NUMBER, showHymnNumbers);

		if (launcherExtra != null) {
            e.putString(LAUNCHER_EXTRA, launcherExtra);
            e.putString(LANGUAGE_EXTRA, languageExtra);
        }

        e.putString(FILTER_TEXT, filterText);

        return (e.commit());
	}

	public static HymnLyricsMO setFavoriteFlag(int hymnId, boolean favorite) {
		HymnLyricsService service = new HymnLyricsService(context);
		HymnLyricsMO hymn = service.retrieveById(hymnId);
		if (favorite) {
			hymn.setFavorite(false);
			Toast.makeText(context,
					context.getString(R.string.Remove_from_favorites),
					Toast.LENGTH_LONG).show();
		} else {
			hymn.setFavorite(true);
			Toast.makeText(context,
					context.getString(R.string.Added_to_favorites),
					Toast.LENGTH_LONG).show();
		}
		service.update(hymn);

		return hymn;
	}

	public static Intent createShareIntent(String title, String author,
			String lyrics) {

		String text = "";
		if (author != null && author.length() > 0) {
			text += author + "\n\n";
		}

		if (HymnLyricsPlusCoreActivity.refrainBetweenStanzas) {
			lyrics = StanzaUtil.refrainBetweenAllStanzas(lyrics);
		}

		if (HymnLyricsPlusCoreActivity.showChords) {
			text += ChordUtil.showChords(lyrics);
		} else {
			text += ChordUtil.stripChords(lyrics);

		}

		text += "\n\n" + context.getString(R.string.Share_message);
		text += "\n" + "https://play.google.com/store/apps/details?id="
				+ packageName;

		Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);

		shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		// set the type
		shareIntent.setType("text/plain");

		// add a subject
		shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, title);
		// add the message
		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);

		return shareIntent;
	}

	public void shareHymn(String title, String author, String lyrics) {

		try {
			Intent shareIntent = createShareIntent(title, author, lyrics);

			Intent chooser = Intent.createChooser(shareIntent, title);
			chooser.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			startActivity(chooser);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(context, context.getString(R.string.Share_error),
					Toast.LENGTH_LONG).show();
		}
	}

	public static void startVideo(String hymnTitle) {
		hymnTitle = TextUtils.htmlEncode(hymnTitle);
		hymnTitle = StringUtil.stripHymnNumber(hymnTitle); 
//		try {
//			Intent intent = new Intent(Intent.ACTION_SEARCH);
//			intent.setPackage("com.google.android.youtube");
//			intent.putExtra("query", hymnTitle);
//			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//			context.startActivity(intent);
//		} catch (Exception e) {
			try {
				String request = "https://m.youtube.com/results?q=" + hymnTitle + " " + youtubeSearch;
				Intent intent = new Intent(Intent.ACTION_VIEW,
						Uri.parse(request));
				mActivity.startActivity(intent);
			} catch (Exception e2) {
				e2.printStackTrace();
				Toast.makeText(context,
						context.getString(R.string.Youtube_error),
						Toast.LENGTH_LONG).show();
			}
//		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setLocale();
	}

	private void showCustomSongsMenu() {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
				HymnLyricsPlusCoreActivity.this);

		ArrayList<String> list = new ArrayList<String>();
		list.add(getString(R.string.Add_Song));
		list.add(getString(R.string.Load_Songs));
		list.add(getString(R.string.Backup_Songs));

		final CharSequence[] options = (CharSequence[]) list
				.toArray(new CharSequence[list.size()]);

		dialogBuilder.setTitle(getString(R.string.Custom_Songs));
		dialogBuilder.setItems(options, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) {
				if (options[item].equals(getString(R.string.Add_Song))) {
					songId = -1;
					showDialog(DIALOG_CUSTOM_SONG);
				} else if (options[item]
						.equals(getString(R.string.Show_favorites))) {
					filterMode = FILTER_MODE_FAVORITES;
					setFilterCriteriaText(FILTER_MODE_FAVORITES, null);
					hymnListAdapter.retrieveFavorites(sortBy);
					hymnListAdapter.notifyDataSetChanged();
				} else if (options[item]
						.equals(getString(R.string.Filter_by_category))) {
					filterMode = FILTER_MODE_CATEGORY;
					showDialog(DIALOG_CATEGORY);
				} else if (options[item]
						.equals(getString(R.string.Backup_Songs))) {
                    Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("text/*");
                    startActivityForResult(intent, ACTIVITY_CHOOSE_SAVE_FILE);
				} else if (options[item].equals(getString(R.string.Load_Songs))) {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("text/*");
                    startActivityForResult(intent, ACTIVITY_CHOOSE_LOAD_FILE);
				}
			}
		});
		dialogBuilder.setCancelable(true);
		dialogBuilder.create().show();
	}

    @SuppressWarnings("unchecked")
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Uri dataUri = data.getData();
            switch (requestCode) {
                case ACTIVITY_CHOOSE_LOAD_FILE:
                    List<Uri> files = new ArrayList<>();
                    files.add(dataUri);
                    loadCustomSongs(files);
                    break;
                case ACTIVITY_CHOOSE_SAVE_FILE:
                    backupCustomSongs(dataUri);
			}
		}
	}

	private void loadCustomSongs(List<Uri> files) {

		progressDialog = new ProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setMessage(getString(R.string.Loading_custom_data));
		progressDialog.show();

		Thread customLoaderThread = new CustomLoaderThread(files,
				customLoaderHandler);
		customLoaderThread.start();
	}

	protected ImportResult loadCustomSong(Uri uri) throws Exception {

		HymnLyricsService service = new HymnLyricsService(
				getApplicationContext());

		InputStream in = getContentResolver().openInputStream(uri);
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		           
		String line;
		int insertCount = 0;
		int updateCount = 0;
		int favoriteCount = 0;
		boolean endOfHymn = false;
		boolean endOfFile = false;
		boolean lyricsFlag = false;
		HymnLyricsMO hymnMO = new HymnLyricsMO();
		String title = "";
		String author = "";
		String keywords = "";
		StringBuffer lyrics = new StringBuffer();
		Integer hymnNumber = null;
		int hymnNumberCount = 0;

		do {
			line = reader.readLine();
			if (line != null) {
                String lowerLine = line.toLowerCase();
				if (lowerLine.startsWith("delete built")) {
					service.deleteAllExceptCustom();
					continue;
				}
				else if (lowerLine.startsWith("delete custom")) {
					service.deleteCustom();
					continue;
				}
				else if (lowerLine.startsWith("delete all")) {
					service.deleteAll();
					continue;
				}
				else if (lowerLine.startsWith("clear favorites")) {
					service.clearFavorites();
					continue;
				}
				else if (lowerLine.startsWith("clear numbers")) {
					service.clearNumbers();
					continue;
				}
				else if (lowerLine.startsWith("clear favorite:")) {
					title = line.substring(15).trim();
					hymnMO = service.retrieveByTitle(title);
					hymnMO.setFavorite(false);
					service.update(hymnMO);
					title = null;
					continue;
				} 
				else if (lowerLine.startsWith("favorite:")) {
					title = line.substring(9).trim();
					hymnMO = service.retrieveByTitle(title);
					hymnMO.setFavorite(true);
					service.update(hymnMO);
					favoriteCount++;
					title = null;
					continue;
				}
				else if (lowerLine.startsWith("title:")) {
					title = line.substring(6).trim();
				} else if (lowerLine.startsWith("keywords:")) {
					keywords = line.substring(9).trim();
				} else if (lowerLine.startsWith("author:")) {
					author = line.substring(7).trim();
				} else if (lowerLine.startsWith("lyrics:")) {
					lyricsFlag = true;
				} else if (lowerLine.startsWith("number:")) {
					try {
						if (line.length() > 7) {
							hymnNumber = new Integer(line.substring(7).trim());
							hymnNumberCount++;
						} else {
							hymnNumber = null;
						}
					} catch (Exception e) {
						hymnNumber = null;
					}
				} else if (line.startsWith("---")) {
					endOfHymn = true;
				} else if (lyricsFlag) {
					lyrics.append(line);
					lyrics.append("\n");
				}
			} else {
				endOfFile = true;
			}

			if (keywords.length() > 0 && !keywords.endsWith(",")) {
				keywords += ",";
			}

			if ((endOfHymn || endOfFile) && title != null) {
				hymnMO = service.retrieveByTitle(title);
				if (hymnMO == null) {
					hymnMO = new HymnLyricsMO();
					hymnMO.setTitle(title);
					hymnMO.setAuthor(author);
					hymnMO.setKeywords(keywords);
					hymnMO.setLyrics(lyrics.toString());
					hymnMO.setUserSong(true);
					hymnMO.setHymnNumber(hymnNumber);
					service.create(hymnMO);
					insertCount++;
				} else if (hymnMO.isUserSong()) {
					if (author.length() > 0) {	
						hymnMO.setAuthor(author);
					}
					if (keywords.length() > 0) {
						hymnMO.setKeywords(keywords);
					}
					if (lyrics.length() > 0) {
						hymnMO.setLyrics(lyrics.toString());
					}
					if (hymnNumber != null) {
						hymnMO.setHymnNumber(hymnNumber);
					}
					service.update(hymnMO);
					updateCount++;
				} else {
					if (hymnNumber != null && hymnNumber.intValue() != 0) {
						hymnMO.setHymnNumber(hymnNumber);
					}
					service.update(hymnMO);
					updateCount++;
				}

				endOfHymn = false;
				lyricsFlag = false;
				lyrics = new StringBuffer();
				hymnNumber = null;
				title = null;
			}
		} while (!endOfFile);

		ImportResult importResult = new ImportResult();
		importResult.insertCount = insertCount;
		importResult.updateCount = updateCount;
		importResult.favoriteCount = favoriteCount;
		importResult.hymnNumberCount = hymnNumberCount;

		reader.close();

        hymnLyricsData.setCategories(service.getAllKeywords());

        return importResult;
	}

	private void setLocale() {
		if (locale != null) {
			Locale.setDefault(locale);
			Configuration config = new Configuration();
			config.locale = locale;
			getBaseContext().getResources().updateConfiguration(config,
					getBaseContext().getResources().getDisplayMetrics());
		}
	}

	private class DialogListSetTypeFace implements OnShowListener {

		@Override
		public void onShow(DialogInterface alert) {
			ListView listView = ((AlertDialog) alert).getListView();
			final ListAdapter originalAdapter = listView.getAdapter();

			listView.setAdapter(new ListAdapter() {

				@Override
				public int getCount() {
					return originalAdapter.getCount();
				}

				@Override
				public Object getItem(int id) {
					return originalAdapter.getItem(id);
				}

				@Override
				public long getItemId(int id) {
					return originalAdapter.getItemId(id);
				}

				@Override
				public int getItemViewType(int id) {
					return originalAdapter.getItemViewType(id);
				}

				@Override
				public View getView(int position, View convertView,
						ViewGroup parent) {
					View view = originalAdapter.getView(position, convertView,
							parent);
					TextView textView = (TextView) view;
					textView.setTypeface(HymnLyricsPlusCoreActivity.typeface);
					return view;
				}

				@Override
				public int getViewTypeCount() {
					return originalAdapter.getViewTypeCount();
				}

				@Override
				public boolean hasStableIds() {
					return originalAdapter.hasStableIds();
				}

				@Override
				public boolean isEmpty() {
					return originalAdapter.isEmpty();
				}

				@Override
				public void registerDataSetObserver(DataSetObserver observer) {
					originalAdapter.registerDataSetObserver(observer);

				}

				@Override
				public void unregisterDataSetObserver(DataSetObserver observer) {
					originalAdapter.unregisterDataSetObserver(observer);

				}

				@Override
				public boolean areAllItemsEnabled() {
					return originalAdapter.areAllItemsEnabled();
				}

				@Override
				public boolean isEnabled(int position) {
					return originalAdapter.isEnabled(position);
				}

			});

		}

	}

	// private class OnScaleGestureListener extends SimpleOnScaleGestureListener
	// {
	//
	// @Override
	// public boolean onScale(ScaleGestureDetector detector) {
	// HymnLyricsPlusCoreActivity.scaleFactor *= detector.getScaleFactor();
	// HymnLyricsPlusCoreActivity.scaleFactor = Math.max(0.1f,
	// Math.min(HymnLyricsPlusCoreActivity.scaleFactor, 5.0f));
	// HymnLyricsPlusCoreActivity.hymnListAdapter.notifyDataSetChanged();
	// return true;
	// }
	//
	// @Override
	// public boolean onScaleBegin(ScaleGestureDetector detector) {
	// return true;
	// }
	//
	// @Override
	// public void onScaleEnd(ScaleGestureDetector detector) {
	// }
	//
	// }

	private class ImportResult {
		public int insertCount;
		public int updateCount;
		public int favoriteCount;
		public int hymnNumberCount;
	}

	void setFilterCriteriaText(int filterMode, String filterText) {

        HymnLyricsPlusCoreActivity.filterText = filterText;

		TextView textView = (TextView) findViewById(R.id.filterCriteria);
		String filterCriteria = "";

		switch (filterMode) {

		case FILTER_MODE_ALL:
			filterCriteria = getString(R.string.Show_all);
			break;

		case FILTER_MODE_FAVORITES:
			filterCriteria = getString(R.string.Favorites);
			break;

		case FILTER_MODE_TITLE:
			filterCriteria = getString(R.string.Title) + ":" + filterText;
			break;

		case FILTER_MODE_LYRICS:
			filterCriteria = getString(R.string.Lyrics) + ":" + filterText;
			break;

		case FILTER_MODE_CATEGORY:
			filterCriteria = getString(R.string.Category) + ":" + filterText;
			break;

		case FILTER_MODE_AUTHOR:
			filterCriteria = getString(R.string.Author) + ":" + filterText;
			break;

		case FILTER_MODE_CHORDS:
			filterCriteria = getString(R.string.Songs_with_chords);
			break;

		case FILTER_MODE_POPULAR_WITH_NO_CHORDS:
			filterCriteria = getString(R.string.Popular_with_no_chords);
			break;

		case FILTER_MODE_CUSTOM_SONGS:
			filterCriteria = getString(R.string.Custom_Songs);
			break;

		case FILTER_MODE_BUILTIN_SONGS:
			filterCriteria = getString(R.string.Builtin_songs);
			break;

		}

		textView.setText(filterCriteria);
	}


	public void hideKeyboard() {
	    InputMethodManager imm = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
	    //Find the currently focused view, so we can grab the correct window token from it.
	    View view = this.getCurrentFocus();
	    //If no view currently has focus, create a new one, just so we can grab a window token from it
	    if (view == null) {
	        view = new View(this);
	    }
	    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
}

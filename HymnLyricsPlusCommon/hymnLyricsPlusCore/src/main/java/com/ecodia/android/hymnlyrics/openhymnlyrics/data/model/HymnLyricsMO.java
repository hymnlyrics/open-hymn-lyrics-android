package com.ecodia.android.hymnlyrics.openhymnlyrics.data.model;

public class HymnLyricsMO {
	private Integer id;
	private String title;
	private String sortTitle;
	private String lastUpdate;
	private String author;
	private String year;
	private String lyrics;
	private String background;
	private String keywords;
	private boolean favorite;
	private String tune;
	private boolean hasChords;
	private boolean userSong;
	private Integer hymnNumber;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getLyrics() {
		return lyrics;
	}

	public void setLyrics(String lyrics) {
		this.lyrics = lyrics;
	}

	public String getBackground() {
		return background;
	}

	public void setBackground(String background) {
		this.background = background;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public boolean isFavorite() {
		return favorite;
	}

	public void setFavorite(boolean favorite) {
		this.favorite = favorite;
	}

	public String getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getTune() {
		return tune;
	}

	public void setTune(String tune) {
		this.tune = tune;
	}


	public boolean hasChords() {
		return hasChords;
	}

	public void setHasChords(boolean hasChords) {
		this.hasChords = hasChords;
	}

	public boolean isUserSong() {
		return userSong;
	}

	public void setUserSong(boolean userSong) {
		this.userSong = userSong;
	}

	public Integer getHymnNumber() {
		return hymnNumber;
	}

	public void setHymnNumber(Integer hymnNumber) {
		this.hymnNumber = hymnNumber;
	}

	public String getSortTitle() {
		return sortTitle;
	}

	public void setSortTitle(String sortTitle) {
		this.sortTitle = sortTitle;
	}
}
